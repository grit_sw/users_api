#!/bin/sh

source venv/bin/activate

sleep 5

export FLASK_APP=run_api.py

echo 'Not Running upgrade'
# echo 'Running upgrade'
# flask db upgrade

# exec flask run -p 5000
exec gunicorn -b :5000 -k eventlet --access-logfile - --error-logfile - run_api:app --workers $NUM_WORKERS --timeout $TIMEOUT
