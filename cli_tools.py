import subprocess
import sys

from dotenv import load_dotenv

from flask import Flask

env_path = '.flaskenv'
load_dotenv(dotenv_path=env_path)


app = Flask(__name__)


@app.cli.command()
def start_api():
    """API Server."""
    ret = subprocess.call(
        ['gunicorn', '-k', 'eventlet', '-b', ':5200', 'run_api:app']
    )
    sys.exit(ret)


@app.cli.command()
def test():
    """Test runner."""
    ret = subprocess.call(
        # ['python', '-m', 'unittest']
        'python -m unittest'.split(" ")
    )
    sys.exit(ret)
