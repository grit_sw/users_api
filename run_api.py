# Test Server.
from os import environ

from arrow import now

from flask import request
from flask_migrate import Migrate

from api import create_api, db
from api.models import Facility, Role, Users
from logger import logger

if environ.get('FLASK_ENV') is None:
    print('FLASK_ENV not set')
mode = environ.get('FLASK_ENV') or 'Staging'
app = create_api(mode)
migrate = Migrate(app, db)


# ===============================================================
# CLI TOOLS
@app.cli.command()
def test():
    """run tests"""
    logger.info('Running tests ...')

# ===============================================================


@app.after_request
def log_info(response):
    try:
        log_data = {
            'connection': request.headers.get('Connection'),
            'ip_address': request.remote_addr,
            'browser_name': request.user_agent.browser,
            'user_device': request.user_agent.platform,
            'referrer': request.referrer,
            'request_url': request.url,
            'host_url': request.host_url,
            'status_code': response.status_code,
            'date': str(now('Africa/Lagos')),
            'location': response.location,
        }
        logger.info('users_api_logs : {}'.format(log_data))
    except Exception as e:
        logger.exception("users_api_logs: {}".format(e))
    return response


with app.app_context():
    # db.reflect()
    # db.drop_all()
    db.create_all()
    db.engine.dialect.supports_sane_rowcount = db.engine.dialect.supports_sane_multi_rowcount = False
    # upgrade()


with app.app_context():
    roles = Role.query.all()
    if not roles:
        logger.info('Inserting Roles')
        Role.insert_roles()

if mode != 'Production':
    from fake import populate_database
    populate_database(app)

if mode == 'Production':
    with app.app_context():
        roles = Role.query.all()
        if len(roles) != 6:
            logger.exception('Invalid Role Creation.')


with app.app_context():
    try:
        migrate_old_users = int(environ.get('MIGRATE_OLD_USERS'))
        if migrate_old_users:
            print('Migrating old users')
            from api.models.old_user import drive
            drive()
            # todo uncomment to migrate users
    except TypeError:
        logger.info('Not migrating')


if mode != 'Production':
    with app.app_context():
        #         # ProsumerManager().new_prosumer('c9ce6c9b-78bd-45d1-96d3-6c8547f8d93d', '140557a9-a625-46c0-90f9-e81eb648f836')
        moyo = Users.query.filter_by(email='moyo@grit.systems').first()
        idarlington83 = Users.query.filter_by(
            email='idarlington@grit.systems').first()
        print(idarlington83)
        if idarlington83:
            print(idarlington83.to_dict())
        if not moyo:
            from fake import create_temp_admin
            create_temp_admin(
                'Moyo',
                'Abudu',
                'moyo@grit.systems',
                '08056041614',
                role_id=5)
        else:
            print('\n\n\nMoyo', moyo.user_id)
        ikenna = Users.query.filter_by(email='ikenna@grit.systems').first()
        if not ikenna:
            from fake import create_temp_admin
            create_temp_admin(
                'Ikenna',
                'Enebuse',
                'ikenna@grit.systems',
                '08056042614',
                role_id=5)
        else:
            print('Ikenna', ikenna.user_id)

        workshop = Users.query.filter_by(email='workshop@grit.systems').first()
        if not workshop:
            from fake import create_temp_admin
            create_temp_admin(
                'GRIT',
                'Workshop',
                'workshop@grit.systems',
                '08056042614',
                role_id=5)
        else:
            print('Ikenna', workshop.user_id)

        user = Users.query.filter_by(email='workshop@grit.systems').first()
        if user:
            print('Workshop', user.user_id)
        else:
            from fake import create_temp_admin
            create_temp_admin(
                'Workshop',
                'Admin',
                'workshop@grit.systems',
                '08054545454',
                role_id=5)

        test_user = Users.query.filter_by(email='test@grit.systems').first()
        if test_user:
            print('Test', 'User', test_user.user_id)
        else:
            from fake import create_temp_admin
            create_temp_admin(
                'Test',
                'Admin',
                'test@grit.systems',
                '08054545954',
                role_id=2)

        for f in Facility.query.all():
            print(f.facility_id, f.name)
