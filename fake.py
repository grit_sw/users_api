from random import choice

from forgery_py.forgery.name import first_name as f_name, last_name as l_name

from api import db
from api.models import Facility, Role, Users
from logger import logger


def create_temp_admin(
        first,
        last,
        email,
        phone_number,
        facility=None,
        role_id=5):
    new_admin = Users(email=email,
                      role_id=role_id,
                      facility_id=facility,
                      parent_id=None,
                      use_mfa=False,
                      use_keypad=True,
                      is_active=True)
    new_admin.create_user(
        first_name=first.title() if first != 'GRIT' else first.upper(),
        last_name=last.title(),
        email=email,
        password='CuAgAu',
        phone_number=phone_number)
    db.session.add(new_admin)
    db.session.commit()
    print('Created Master admin')


def populate_database(app):
    with app.app_context():
        print('Counting Users')
        _count = Users.query.count()
        print('{} users in database'.format(_count))
        if _count < 2:
            try:
                db.session.close()
                db.drop_all()
                db.create_all()
            except Exception as e:
                logger.exception(e)
                db.session.rollback()
                exit()
            Role.insert_roles()
            new_facility_users()
            _count = Users.query.count()
            print('{} users in database'.format(_count))
            print('Facility details = ', Facility.all())


def generate_roles():
    Role.insert_roles()


def create_unique_items(count, func, arg=None):
    seq = []
    while True:
        if arg:
            r = list({func(arg) for _ in range(count)})
        else:
            r = list({func() for _ in range(count)})
        if len(r) == count:
            seq = r
            break
        # print(f'Running {r} {seq}')
    return seq


def create_facilities():
    # names = create_unique_items(40, f_name)
    # addresses = create_unique_items(40, street_address)
    # types = [choice(['Market', 'School', 'Church', 'Mosque',
    #                'Shop', 'House', 'Office']) for _ in range(20)]
    facilities = {
       'facilities': [
            dict(
            name='Sura',
            country="Nigeria",
            state="Lagos",
            city="Ikeja",
            street="Helper lane",
            house_no="1",
            billing_method="kW/h",
            billing_unit="k",
            facility_type='Market'
            ),
            dict(
            name='Ariaria',
            country="Nigeria",
            state="Lagos",
            city="Ikeja",
            street="Helper lane",
            house_no="1",
            billing_method="kW/h",
            billing_unit="k",
            facility_type='Market'
            ),
            dict(
            name='AsoRock',
            country="Nigeria",
            state="Lagos",
            city="Ikeja",
            street="Helper lane",
            house_no="1",
            billing_method="kW/h",
            billing_unit="k",
            facility_type='Office')]
    }

    for facility in facilities['facilities']:
        Facility.create_facility(facility)


def add_users_to_facility(
        mgt_admins,
        client_admins,
        user_admins,
        parent_id,
        facility_id):
    for _ in range(mgt_admins):
        new_user(facility_id=facility_id, parent_id=parent_id, role_id=4)
    for _ in range(client_admins):
        new_user(facility_id=facility_id, parent_id=parent_id, role_id=3)
    for _ in range(user_admins):
        new_user(facility_id=facility_id, parent_id=parent_id, role_id=2)


def new_facility_users():
    create_facilities()
    master_email = new_master_admin()
    master_admin = Users.query.filter_by(email=master_email).first()
    num_facility_admin = 2
    num_client_admins = 3
    num_user_admins = 3
    num_users = 2

    facilities = Facility.query.all()
    for _ in range(num_facility_admin):
        # Create Management Admins
        facility_id = facilities[0].facility_id
        role_id = 4
        parent_id = master_admin.user_id
        email = new_user(role_id, facility_id, parent_id)['email']
        mgt_admin = Users.get_user(email=email)
        for _ in range(num_client_admins):
            # Create Client Admins
            role_id = 3
            parent_id = mgt_admin.user_id
            email = new_user(role_id, facility_id, parent_id)['email']
            client_admin = Users.get_user(email=email)
            # print('New User {} with role {}'.format(email, client_admin.role))
            for _ in range(num_user_admins):
                # Create User Admins
                role_id = 2
                parent_id = client_admin.user_id
                email = new_user(role_id, facility_id, parent_id)['email']
                Users.get_user(email=email)
                for _ in range(num_users):
                    # Create Users
                    role_id = 1
                    parent_id = client_admin.user_id
                    email = new_user(role_id, facility_id, parent_id)['email']
                    Users.get_user(email=email)
        facilities.pop(0)
        _count = Users.query.count()
        print('\n\n\n{} users in database'.format(_count))


def _password():
    """Generates an alpha-numeric password"""
    # return ''.join(choice(chars) for x in range(size))
    return 'haha'


def p_number():
    """Generates random phonenumbers"""
    return '0{a}{b}{c}{d}'.format(a=str(choice([7, 8])), b=str(choice([0, 1])), c=str(
        choice([2, 9])), d=str(''.join(str(choice(list(range(0, 9)))) for _ in range(8))))


def mail_vendors():
    return ['yahoo.com', 'yahoo.co.uk', 'hotmail.com', 'gmail.com']


def user_email(first, last):
    return '{first}.{last}@{email}'.format(
        first=first.lower(),
        last=last.lower(),
        email=choice(
            mail_vendors()))


def generate_user(role_id, facility_id=None, parent_id=None):
    users = dict()
    first_name = f_name()
    last_name = l_name()
    email = user_email(first_name, last_name)
    phone_number = p_number()
    users['first_name'] = first_name
    users['last_name'] = last_name
    users['email'] = email
    users['password'] = 'haha'
    users['phone_number'] = phone_number
    users['role_id'] = role_id
    users['facility_id'] = facility_id
    users['parent_id'] = parent_id
    users['use_mfa'] = False
    users["is_active"] = True
    return users


def new_user_params(user):
    params = {}
    params['email'] = user['email']
    params['role_id'] = user['role_id']
    params['facility_id'] = user['facility_id']
    params['parent_id'] = user['parent_id']
    params['use_mfa'] = user['use_mfa']
    params['is_active'] = user['is_active']
    return params


def new_grit_admin(user):
    user['first_name'] = 'GRIT'
    user['last_name'] = 'ADMIN'
    user['email'] = 'workshop@grit.systems'
    return user


def sign_up(user):
    params = {}
    params['first_name'] = user['first_name']
    params['last_name'] = user['last_name']
    params['email'] = user['email']
    params['password'] = user['password']
    params['phone_number'] = user['phone_number']
    return params


def sign_up_grit_admin():
    create_facilities()
    generate_roles()
    role_id = 5
    user = generate_user(role_id)
    user = new_grit_admin(user)
    admin_invite_params = new_user_params(user)
    admin_signup_params = sign_up(user)
    new_admin = Users(**admin_invite_params)
    new_admin.create_user(**admin_signup_params)
    db.session.add(new_admin)
    db.session.commit()
    db.session.refresh(new_admin)
    return new_admin.to_dict()


def new_user(role_id, facility_id, parent_id, is_test=False):
    user = generate_user(role_id, facility_id, parent_id)
    new = new_user_params(user)
    user_params = sign_up(user)
    register = Users(**new)
    register.create_user(**user_params)
    db.session.add(register)
    db.session.commit()
    db.session.refresh(register)
    if is_test:
        return register.to_dict()
    return register.to_dict()


def new_master_admin():
    role_id = 5
    user = generate_user(role_id)
    user = new_grit_admin(user)
    admin_invite_params = new_user_params(user)
    admin_signup_params = sign_up(user)
    # print(admin_signup_params)
    new_admin = Users(**admin_invite_params)
    new_admin.create_user(**admin_signup_params)
    db.session.add(new_admin)
    db.session.commit()
    return user['email']
