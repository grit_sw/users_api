from random import choice
import unittest

from api import create_api, db
from fake import sign_up_grit_admin, new_user, add_users_to_facility


def create_random_grit_admin():
    user = sign_up_grit_admin()
    return user


def create_random_user(grit_admin, user, facility=None):
    parent_id = grit_admin['user_id']
    if facility is None:
        facility_id = choice(grit_admin['facilities'])['facility_id']
    else:
        facility_id = grit_admin['facilities'][facility]['facility_id']

    if user == 'mgt_admin':
        role_id = 4
    elif user == 'client_admin':
        role_id = 3
    elif user == 'user_admin':
        role_id = 2
    else:
        role_id = 1
    generated = new_user(
        role_id=role_id,
        parent_id=parent_id,
        facility_id=facility_id)
    return generated


def two_users_in_different_facilities_with_same_role(grit_admin):
    parent_id = grit_admin['user_id']
    facilities = grit_admin['facilities']
    facility_id_1 = facilities[0]['facility_id']
    facility_id_2 = facilities[1]['facility_id']
    first_user = new_user(
        role_id=3,
        parent_id=parent_id,
        facility_id=facility_id_1)
    second_user = new_user(
        role_id=3,
        parent_id=parent_id,
        facility_id=facility_id_2)

    return first_user, second_user


class UsersTest(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.api = create_api('Testing')
        cls.ctx = cls.api.app_context()
        cls.ctx.push()
        db.drop_all()
        db.create_all()

    def setUp(self):
        self.client = self.api.test_client()
        self.fake_user_id = 'fake_user_id'
        self.fake_role_id = 7
        self.fake_facility_id = 'fake_facility_id'
        self.fake_group_id = 'fake_group_id'

    def tearDown(self):
        pass

    @classmethod
    def tearDownClass(cls):
        db.session.remove()
        db.drop_all()
        cls.ctx.pop()

    def test_search(self):
        url = '/users/search/'  # /users/search/

        grit_admin = create_random_grit_admin()

        # Create non grit users in the same facility; set 1
        create_random_user(
            grit_admin, user='mgt_admin', facility=0)

        client_admin = create_random_user(
            grit_admin, user='client_admin', facility=0)
        client_admin_id = client_admin['user_id']

        user_admin = create_random_user(
            grit_admin, user='user_admin', facility=0)
        user_admin_id = user_admin['user_id']

        base_user = create_random_user(grit_admin, user='user', facility=0)
        base_user_id = base_user['user_id']
        first_name = base_user['first_name']
        role_id = base_user['role_id']
        facility_id = base_user['facilities'][0]['facility_id']

        # Request with unauthorized operations
        data = {
            'query': str(first_name),
            'role_id': str(role_id),
            'facility_id': str(facility_id)
        }

        self.client.set_cookie(url, 'no_user_id', self.fake_user_id)
        search = self.client.get(url, query_string=data)
        self.assertEqual(search.status_code, 403)

        self.client.set_cookie(url, 'user_id', self.fake_user_id)
        search = self.client.get(url, query_string=data)
        self.assertEqual(search.status_code, 404)

        self.client.set_cookie(url, 'user_id', user_admin_id)
        search = self.client.get(url, query_string=data)
        self.assertEqual(search.status_code, 403)

        self.client.set_cookie(url, 'user_id', base_user_id)
        search = self.client.get(url, query_string=data)
        self.assertEqual(search.status_code, 403)

        # Request with invalid role and valid facility id
        data = {
            'query': str(first_name),
            'role_id': 'string',
            'facility_id': str(facility_id)
        }

        self.client.set_cookie(url, 'user_id', client_admin_id)
        search = self.client.get(url, query_string=data)
        self.assertEqual(search.status_code, 400)

        # Request with valid role and no facility id
        data = {
            'query': str(first_name),
            'role_id': str(role_id)
        }

        self.client.set_cookie(url, 'user_id', client_admin_id)
        search = self.client.get(url, query_string=data)
        self.assertEqual(search.status_code, 400)

        # Request with no role and valid facility id
        data = {
            'query': str(first_name),
            'facility_id': str(facility_id)
        }

        self.client.set_cookie(url, 'user_id', client_admin_id)
        search = self.client.get(url, query_string=data)
        self.assertEqual(search.status_code, 400)

        # Request with valid role and valid facility id
        data = {
            'query': str(first_name),
            'role_id': str(role_id),
            'facility_id': str(facility_id)
        }

        self.client.set_cookie(url, 'user_id', client_admin_id)
        search = self.client.get(url, query_string=data)
        self.assertEqual(search.status_code, 200)

    def test_edit_me(self):
        url = '/users/edit-me/'  # /users/edit-me/

        grit_admin = create_random_grit_admin()
        grit_admin_id = grit_admin['user_id']

        payload = {
            'user_id': grit_admin_id,
            'first_name': 'Sulemanjohnson',
            'last_name': 'Johnsonsuleman',
            'email': 'Sulemanjohnsonjohnsonsuleman@gmail.com',
            'phone_number': '08101110111',
            'use_mfa': not grit_admin['mfa_user']
        }

        resp = self.client.put(url)
        self.assertEqual(resp.status_code, 403)

        # Request by user with fake user id
        self.client.set_cookie(url, 'user_id', self.fake_user_id)
        me = self.client.put(url, data=payload)
        self.assertEqual(me.status_code, 404)

        # Request by a valid user with valid payload
        self.client.set_cookie(url, 'user_id', grit_admin_id)
        me = self.client.put(url, data=payload)
        self.assertEqual(me.status_code, 200)
        self.assertEqual(me.json['data']['first_name'], payload['first_name'])
        self.assertEqual(me.json['data']['last_name'], payload['last_name'])
        self.assertEqual(me.json['data']['mfa_user'], payload['use_mfa'])

        # Request made with an incomplete payload
        del payload['user_id']
        self.client.set_cookie(url, 'user_id', grit_admin_id)
        me = self.client.put(url, data=payload)
        self.assertEqual(me.status_code, 400)

    def test_edit_user(self):
        url = '/users/edit-user/'

        grit_admin = create_random_grit_admin()
        grit_admin_id = grit_admin['user_id']

        mgt_admin = create_random_user(grit_admin, user='mgt_admin')
        mgt_admin_id = mgt_admin['user_id']

        client_admin = create_random_user(grit_admin, user='client_admin')
        client_admin_id = client_admin['user_id']

        user_admin = create_random_user(grit_admin, user='user_admin')
        user_admin_id = user_admin['user_id']

        base_user = create_random_user(grit_admin, user='user')
        base_user_id = base_user['user_id']

        user_to_edit = create_random_user(grit_admin, user='user')
        user_to_edit_id = user_to_edit['user_id']

        payload = {
            'user_id': user_to_edit_id,
            'first_name': 'Sulemanjohnson',
            'last_name': 'Johnsonsuleman',
            'email': 'Sulemanjohnsonjohnsonsuleman@gmail.com',
            'phone_number': '08101110111',
            'use_mfa': not user_to_edit['mfa_user'],
            'use_keypad': not user_to_edit['keypad_user']
        }

        # Request without user id in cookies
        resp = self.client.put(url)
        self.assertEqual(resp.status_code, 403)

        # Request by user with fake user id
        self.client.set_cookie(url, 'user_id', self.fake_user_id)
        me = self.client.put(url, data=payload)
        self.assertEqual(me.status_code, 403)

        # Request by base user
        self.client.set_cookie(url, 'user_id', base_user_id)
        me = self.client.put(url, data=payload)
        self.assertEqual(me.status_code, 403)

        # Request by user admin
        self.client.set_cookie(url, 'user_id', user_admin_id)
        me = self.client.put(url, data=payload)
        self.assertEqual(me.status_code, 403)

        # Request by a grit user with valid payload
        self.client.set_cookie(url, 'user_id', grit_admin_id)
        me = self.client.put(url, data=payload)
        self.assertEqual(me.status_code, 200)
        self.assertEqual(me.json['data']['first_name'], payload['first_name'])
        self.assertEqual(me.json['data']['last_name'], payload['last_name'])
        self.assertEqual(me.json['data']['mfa_user'], payload['use_mfa'])

        # Request by a mgt admin with valid payload
        payload['first_name'] = 'Chigozie'
        payload['last_name'] = 'Chimobi'
        payload['use_mfa'] = not payload['use_mfa']
        self.client.set_cookie(url, 'user_id', mgt_admin_id)
        me = self.client.put(url, data=payload)
        self.assertEqual(me.status_code, 200)
        self.assertEqual(me.json['data']['first_name'], payload['first_name'])
        self.assertEqual(me.json['data']['last_name'], payload['last_name'])
        self.assertEqual(me.json['data']['mfa_user'], payload['use_mfa'])

        # Request by a client admin with valid payload
        payload['first_name'] = 'Moyosore'
        payload['last_name'] = 'Ifedayo'
        payload['use_mfa'] = not payload['use_mfa']
        self.client.set_cookie(url, 'user_id', client_admin_id)
        me = self.client.put(url, data=payload)
        self.assertEqual(me.status_code, 200)
        self.assertEqual(me.json['data']['first_name'], payload['first_name'])
        self.assertEqual(me.json['data']['last_name'], payload['last_name'])
        self.assertEqual(me.json['data']['mfa_user'], payload['use_mfa'])

        # Request to edit a non existent user
        payload['user_id'] = self.fake_user_id
        self.client.set_cookie(url, 'user_id', grit_admin_id)
        me = self.client.put(url, data=payload)
        self.assertEqual(me.status_code, 400)

        # Request made with an incomplete payload
        del payload['user_id']
        self.client.set_cookie(url, 'user_id', grit_admin_id)
        me = self.client.put(url, data=payload)
        self.assertEqual(me.status_code, 400)

    def test_get_me(self):
        url = '/users/me/'

        resp = self.client.get(url)
        self.assertEqual(resp.status_code, 403)

        self.client.set_cookie(url, 'user_id', self.fake_user_id)
        me = self.client.get(url)
        self.assertEqual(me.status_code, 404)

        user = create_random_grit_admin()
        user_id = user['user_id']
        self.client.set_cookie(url, 'user_id', user_id)
        me = self.client.get(url)
        self.assertEqual(me.status_code, 200)
        self.assertEqual(me.json['data']['user_id'], user_id)

    def test_get_user_by_id(self):
        url = '/users/id/{}/'
        resp = self.client.get(url)
        self.assertEqual(resp.status_code, 403)

        self.client.set_cookie(url, 'user_id', self.fake_user_id)
        user = self.client.get(url)
        self.assertEqual(user.status_code, 403)

        user_to_get = create_random_grit_admin()
        grit_admin_ = create_random_grit_admin()
        mgt_admin = create_random_user(
            grit_admin_,
            user='mgt_admin'
        )
        client_admin = create_random_user(
            grit_admin_,
            user='client_admin'
        )
        user_admin = create_random_user(
            grit_admin_,
            user='user_admin'
        )
        base_user = create_random_user(
            grit_admin_,
            user='user'
        )
        user_to_get_id = user_to_get['user_id']
        grit_admin_id = grit_admin_['user_id']
        mgt_admin_id = mgt_admin['user_id']
        client_admin_id = client_admin['user_id']
        user_admin_id = user_admin['user_id']
        base_user_id = base_user['user_id']

        user_1, user_2 = two_users_in_different_facilities_with_same_role(
            grit_admin_)
        user_1_id = user_1['user_id']
        user_2_id = user_2['user_id']

        # Request by user that doesnt exist.
        self.client.set_cookie(url, 'user_id', self.fake_user_id)
        user = self.client.get(url.format(user_to_get_id))
        self.assertEqual(user.status_code, 403)

        # Get a user that doesnt exist.
        self.client.set_cookie(url, 'user_id', grit_admin_id)
        user = self.client.get(url.format(self.fake_user_id))
        self.assertEqual(user.status_code, 404)

        # Get a user that has a higher role than me 1.
        self.client.set_cookie(url, 'user_id', mgt_admin_id)  # ME
        user = self.client.get(url.format(grit_admin_id))  # request
        self.assertEqual(user.status_code, 403)

        # Get a user that has a higher role than me 2.
        self.client.set_cookie(url, 'user_id', client_admin_id)  # ME
        user = self.client.get(url.format(mgt_admin_id))  # request
        self.assertEqual(user.status_code, 403)

        # Get a user that has a higher role than me 3.
        self.client.set_cookie(url, 'user_id', user_admin_id)  # ME
        user = self.client.get(url.format(client_admin_id))  # request
        self.assertEqual(user.status_code, 403)

        # Get a user that has a higher role than me 4.
        self.client.set_cookie(url, 'user_id', base_user_id)  # ME
        user = self.client.get(url.format(user_admin_id))  # request
        self.assertEqual(user.status_code, 403)

        # Get details of user with same role but in a different facility.
        self.client.set_cookie(url, 'user_id', user_1_id)
        user = self.client.get(url.format(user_2_id))
        self.assertEqual(user.status_code, 403)

        # Get my own details.
        self.client.set_cookie(url, 'user_id', user_1_id)
        user = self.client.get(url.format(user_1_id))
        self.assertEqual(user.status_code, 200)
        self.assertEqual(user.json['data']['user_id'], user_1_id)

        # Get a user as a GRIT admin.
        self.client.set_cookie(url, 'user_id', grit_admin_id)
        user = self.client.get(url.format(user_to_get_id))
        self.assertEqual(user.status_code, 200)
        self.assertEqual(user.json['data']['user_id'], user_to_get_id)

    def test_get_all_users_in_facility(self):
        url = '/users/in-facility/{}/'  # /in-facility/<string:facility_id>/
        num_mgt_admins = 2
        num_client_admins = 1
        num_user_admins = 2
        resp = self.client.get(url)
        self.assertEqual(resp.status_code, 403)

        grit_admin = create_random_grit_admin()
        facility_id = grit_admin['facilities'][0]['facility_id']
        grit_admin_id = grit_admin['user_id']
        add_users_to_facility(
            mgt_admins=num_mgt_admins,
            client_admins=num_client_admins,
            user_admins=num_user_admins,
            parent_id=grit_admin_id,
            facility_id=facility_id,
        )
        client_admin = new_user(
            role_id=3,
            parent_id=grit_admin_id,
            facility_id=facility_id,
        )
        client_admin_id = client_admin['user_id']

        # Request by user that doesnt exist.
        self.client.set_cookie(url, 'user_id', self.fake_user_id)
        user = self.client.get(url.format(facility_id))
        self.assertEqual(user.status_code, 403)

        # Request for a facility that doesn't exist.
        self.client.set_cookie(url, 'user_id', grit_admin_id)
        user = self.client.get(url.format(self.fake_facility_id))
        self.assertEqual(user.status_code, 404)

        # Request for a facility that exists.
        self.client.set_cookie(url, 'user_id', grit_admin_id)
        user = self.client.get(url.format(facility_id))
        self.assertEqual(user.status_code, 200)

        # Request for a facility that exists.
        self.client.set_cookie(url, 'user_id', client_admin_id)
        user = self.client.get(url.format(facility_id))
        self.assertEqual(user.status_code, 200)

    def test_get_all_users(self):
        url = '/users/all/'
        grit_admin = create_random_grit_admin()
        grit_admin_id = grit_admin['user_id']

        non_grit_users = ['mgt_admin', 'client_admin', 'user_admin', 'user']
        random_non_grit_user = choice(non_grit_users)

        non_grit_user = create_random_user(
            grit_admin, user=random_non_grit_user)
        non_grit_user_id = non_grit_user['user_id']

        # Request by user that doesnt exist.
        self.client.set_cookie(url, 'user_id', self.fake_user_id)
        user = self.client.get(url)
        self.assertEqual(user.status_code, 404)

        # Request made by a grit admin
        self.client.set_cookie(url, 'user_id', grit_admin_id)
        user = self.client.get(url)
        self.assertEqual(user.status_code, 200)

        # Request made by a non grit admin
        self.client.set_cookie(url, 'user_id', non_grit_user_id)
        user = self.client.get(url)
        self.assertEqual(user.status_code, 403)

    def test_get_all_roles(self):
        url = '/users/role/all/'
        grit_admin = create_random_grit_admin()
        grit_admin_id = grit_admin['user_id']

        non_grit_users = ['mgt_admin', 'client_admin', 'user_admin', 'user']
        random_non_grit_user = choice(non_grit_users)

        non_grit_user = create_random_user(
            grit_admin, user=random_non_grit_user)
        non_grit_user_id = non_grit_user['user_id']

        # Request by user that doesnt exist.
        self.client.set_cookie(url, 'user_id', self.fake_user_id)
        user = self.client.get(url)
        self.assertEqual(user.status_code, 404)

        # Request made by a grit admin
        self.client.set_cookie(url, 'user_id', grit_admin_id)
        user = self.client.get(url)
        self.assertEqual(user.status_code, 201)

        # Request made by a non grit admin
        self.client.set_cookie(url, 'user_id', non_grit_user_id)
        user = self.client.get(url)
        self.assertEqual(user.status_code, 403)

    def test_get_all_users_in_all_facilities_with_role(self):
        url = '/users/by-role/{}/'  # /by-role/<string:role_id>/

        grit_admin = create_random_grit_admin()
        grit_admin_id = grit_admin['user_id']
        roles = [1, 2, 3, 4, 5]
        role_id = choice(roles)

        non_grit_users = ['mgt_admin', 'client_admin', 'user_admin', 'user']
        random_non_grit_user = choice(non_grit_users)

        non_grit_user = create_random_user(
            grit_admin, user=random_non_grit_user)
        non_grit_user_id = non_grit_user['user_id']

        # Request by user that doesnt exist.
        self.client.set_cookie(url, 'user_id', self.fake_user_id)
        user = self.client.get(url.format(role_id))
        self.assertEqual(user.status_code, 404)

        # Request made by a grit admin
        self.client.set_cookie(url, 'user_id', grit_admin_id)
        user = self.client.get(url.format(role_id))
        self.assertEqual(user.status_code, 201)

        # Request made by a non grit admin
        self.client.set_cookie(url, 'user_id', non_grit_user_id)
        user = self.client.get(url.format(role_id))
        self.assertEqual(user.status_code, 403)

    def test_get_all_users_in_one_facility_with_role(self):
        # /users/by-role-facility/<string:role_id>/<string:facility_id>/
        url = '/users/by-role-facility/{}/{}/'

        grit_admin = create_random_grit_admin()
        grit_admin_id = grit_admin['user_id']

        facilities = [
            grit_admin['facilities'][0]['facility_id'],
            grit_admin['facilities'][1]['facility_id'],
            grit_admin['facilities'][2]['facility_id']
        ]

        roles = [1, 2, 3, 4, 5]

        client_admin = create_random_user(
            grit_admin, user='client_admin', facility=0)
        client_admin_id = client_admin['user_id']
        client_admin_role_id = client_admin['role_id']
        client_admin_facility_id = client_admin['facilities'][0]['facility_id']

        # Request by user that doesnt exist.
        self.client.set_cookie(url, 'user_id', self.fake_user_id)
        user = self.client.get(url.format(choice(roles), choice(facilities)))
        self.assertEqual(user.status_code, 404)

        # Request for a facility that doesn't exist.
        self.client.set_cookie(url, 'user_id', grit_admin_id)
        user = self.client.get(
            url.format(
                choice(roles),
                self.fake_facility_id))
        self.assertEqual(user.status_code, 404)

        # Request made for a facility that does not match
        self.client.set_cookie(url, 'user_id', client_admin_id)
        user = self.client.get(url.format(
            choice(roles[0:2]), choice(facilities[1:])))
        self.assertEqual(user.status_code, 403)

        # Request made for a facility that matches; case 1; same role (grit
        # admin only)
        self.client.set_cookie(url, 'user_id', grit_admin_id)
        user = self.client.get(url.format(5, choice(facilities)))
        self.assertEqual(user.status_code, 200)

        # Request made for a facility that matches; case 2; same roles (non
        # grit admin)
        self.client.set_cookie(url, 'user_id', client_admin_id)
        user = self.client.get(
            url.format(
                client_admin_role_id,
                client_admin_facility_id))
        self.assertEqual(user.status_code, 403)

        # Request made for a facility that matches; case 3; lower roles
        self.client.set_cookie(url, 'user_id', client_admin_id)
        user = self.client.get(url.format(
            choice(roles[0:2]), client_admin_facility_id))
        self.assertEqual(user.status_code, 200)

        # Request made for a facility that matches; case 4; hihger roles
        self.client.set_cookie(url, 'user_id', client_admin_id)
        user = self.client.get(url.format(
            choice(roles[2:]), client_admin_facility_id))
        self.assertEqual(user.status_code, 403)

    def test_get_max_admin_in_group(self):
        url = '/users/max-admin/{}>/'  # /max-admin/<string:group_id>/

        create_random_grit_admin()

        # non_grit_users = ['mgt_admin', 'client_admin', 'user_admin', 'user']
        # random_non_grit_user = choice(non_grit_users)

        # non_grit_user = create_random_user(grit_admin, user=random_non_grit_user)
        # non_grit_user_id = non_grit_user['user_id']

        # Request made with a fake group id.
        # self.client.set_cookie(url, 'user_id', self.fake_user_id)
        user = self.client.get(url.format(self.fake_group_id))
        self.assertEqual(user.status_code, 404)

        # Implement actually getting a max admin in a group later

    def test_get_all_users_in_a_group(self):
        url = '/users/by-group/{}/'  # /by-group/<string:group_id>/

        create_random_grit_admin()

        # non_grit_users = ['mgt_admin', 'client_admin', 'user_admin', 'user']
        # random_non_grit_user = choice(non_grit_users)

        # non_grit_user = create_random_user(grit_admin, user=random_non_grit_user)
        # non_grit_user_id = non_grit_user['user_id']

        # Request by user that doesnt exist.
        self.client.set_cookie(url, 'user_id', self.fake_user_id)
        user = self.client.get(url.format(self.fake_group_id))
        self.assertEqual(user.status_code, 403)

        # Implement actually getting all users in a group later

    def test_get_delete_user(self):
        url = '/users/delete/{}/'  # /delete/<string:user_id>/

        # create grit users
        grit_admin = create_random_grit_admin()
        grit_admin_id = grit_admin['user_id']

        grit_admin_2 = create_random_grit_admin()
        grit_admin_2_id = grit_admin_2['user_id']

        grit_admin_3 = create_random_grit_admin()
        grit_admin_3_id = grit_admin_3['user_id']

        # Create non grit users in the same facility; set 1
        mgt_admin_1 = create_random_user(
            grit_admin, user='mgt_admin', facility=1)
        mgt_admin_1_id = mgt_admin_1['user_id']

        client_admin_1 = create_random_user(
            grit_admin, user='client_admin', facility=1)
        client_admin_1_id = client_admin_1['user_id']

        a_client_admin_1 = create_random_user(
            grit_admin, user='client_admin', facility=1)
        a_client_admin_1_id = a_client_admin_1['user_id']

        user_admin_1 = create_random_user(
            grit_admin, user='user_admin', facility=1)
        user_admin_1_id = user_admin_1['user_id']

        create_random_user(grit_admin, user='user', facility=1)

        # Create non grit users in the same facility; set 2
        mgt_admin_2 = create_random_user(
            grit_admin, user='mgt_admin', facility=2)
        mgt_admin_2_id = mgt_admin_2['user_id']

        client_admin_2 = create_random_user(
            grit_admin, user='client_admin', facility=2)
        client_admin_2_id = client_admin_2['user_id']

        user_admin_2 = create_random_user(
            grit_admin, user='user_admin', facility=2)
        user_admin_2_id = user_admin_2['user_id']

        base_user_2 = create_random_user(grit_admin, user='user', facility=2)
        base_user_2_id = base_user_2['user_id']

        ##################################  SELF DELETES ######################

        grit_admin_4 = create_random_grit_admin()
        grit_admin_4_id = grit_admin_4['user_id']

        create_random_user(
            grit_admin, user='mgt_admin', facility=0)
        a_mgt_admin_1_id = mgt_admin_1['user_id']

        #######################################################################

        # Request to delete a user that doesnt exist.
        self.client.set_cookie(url, 'user_id', grit_admin_id)
        user = self.client.delete(url.format(self.fake_user_id))
        self.assertEqual(user.status_code, 404)

        # Request by a user that doesnt exist.
        self.client.set_cookie(url, 'user_id', self.fake_user_id)
        user = self.client.delete(url.format(grit_admin_id))
        self.assertEqual(user.status_code, 403)

        # Request by a grit admin to delete another grit admin.
        self.client.set_cookie(url, 'user_id', grit_admin_id)
        user = self.client.delete(url.format(grit_admin_3_id))
        self.assertEqual(user.status_code, 200)

        ###############################  SAME FACILITIES ######################

        # Request by a lower role user to delete a user with higher role in
        # same facility;
        self.client.set_cookie(url, 'user_id', user_admin_1_id)
        user = self.client.delete(url.format(client_admin_1_id))
        self.assertEqual(user.status_code, 403)

        # Request to delete a user with same role in same facility;
        self.client.set_cookie(url, 'user_id', client_admin_1_id)
        user = self.client.delete(url.format(a_client_admin_1_id))
        self.assertEqual(user.status_code, 403)

        # Request to delete a user with lower role in same facility;
        self.client.set_cookie(url, 'user_id', client_admin_1_id)
        user = self.client.delete(url.format(user_admin_1_id))
        self.assertEqual(user.status_code, 200)

        #############################  DIFFERENT FACILITIES ###################

        # Request by a lower role user to delete a user with higher role in
        # different facility;
        self.client.set_cookie(url, 'user_id', user_admin_2_id)
        user = self.client.delete(url.format(client_admin_1_id))
        self.assertEqual(user.status_code, 403)

        # Request to delete a user with same role in different facility;
        self.client.set_cookie(url, 'user_id', client_admin_2_id)
        user = self.client.delete(url.format(client_admin_1_id))
        self.assertEqual(user.status_code, 403)

        # Request to delete a user with lower role in different facility; Works but messes with db
        # self.client.set_cookie(url, 'user_id', mgt_admin_2_id)
        # user = self.client.delete(url.format(a_client_admin_1_id))
        # self.assertEqual(user.status_code, 403)

        ##################################  SELF DELETES ######################

        # Request to delete yourself as a grit admin;
        self.client.set_cookie(url, 'user_id', grit_admin_4_id)
        user = self.client.delete(url.format(grit_admin_4_id))
        self.assertEqual(user.status_code, 200)

        # Request to delete yourself as a mgt admin;
        self.client.set_cookie(url, 'user_id', a_mgt_admin_1_id)
        user = self.client.delete(url.format(a_mgt_admin_1_id))
        self.assertEqual(user.status_code, 403)

        # Request to delete yourself as a client admin;
        self.client.set_cookie(url, 'user_id', a_client_admin_1_id)
        user = self.client.delete(url.format(a_client_admin_1_id))
        self.assertEqual(user.status_code, 403)

        # Request to delete yourself as a user_admin;
        self.client.set_cookie(url, 'user_id', user_admin_2_id)
        user = self.client.delete(url.format(user_admin_2_id))
        self.assertEqual(user.status_code, 403)

        # Request to delete yoirself as a base user;
        self.client.set_cookie(url, 'user_id', base_user_2_id)
        user = self.client.delete(url.format(base_user_2_id))
        self.assertEqual(user.status_code, 200)

        #######################################################

        # Request by a grit admin to delete non grit users. case 1a
        self.client.set_cookie(url, 'user_id', grit_admin_id)
        user = self.client.delete(url.format(mgt_admin_1_id))
        self.assertEqual(user.status_code, 200)

        # Request by a grit admin to delete non grit users. case 1b
        self.client.set_cookie(url, 'user_id', grit_admin_id)
        user = self.client.delete(url.format(mgt_admin_2_id))
        self.assertEqual(user.status_code, 200)

        # Request by a grit admin to delete non grit users. case 2a
        self.client.set_cookie(url, 'user_id', grit_admin_2_id)
        user = self.client.delete(url.format(client_admin_2_id))
        self.assertEqual(user.status_code, 200)

        # Request by a grit admin to delete non grit users. case 2b
        self.client.set_cookie(url, 'user_id', grit_admin_2_id)
        user = self.client.delete(url.format(client_admin_1_id))
        self.assertEqual(user.status_code, 200)

    def test_change_user_role(self):
        url = '/users/change-role/'  # /users/change-role/

        grit_admin_1 = create_random_grit_admin()
        grit_admin_1_id = grit_admin_1['user_id']

        grit_admin_2 = create_random_grit_admin()
        grit_admin_2_id = grit_admin_2['user_id']

        # Create non grit users in the same facility; set 1
        mgt_admin_1 = create_random_user(
            grit_admin_1, user='mgt_admin', facility=0)
        mgt_admin_1_id = mgt_admin_1['user_id']

        client_admin_1 = create_random_user(
            grit_admin_1, user='client_admin', facility=0)
        client_admin_1_id = client_admin_1['user_id']

        a_client_admin_1 = create_random_user(
            grit_admin_1, user='client_admin', facility=0)
        a_client_admin_1_id = a_client_admin_1['user_id']

        user_admin_1 = create_random_user(
            grit_admin_1, user='user_admin', facility=0)
        user_admin_1_id = user_admin_1['user_id']

        create_random_user(grit_admin_1, user='user', facility=0)

        # Create non grit users in the same facility; set 2
        create_random_user(
            grit_admin_2, user='mgt_admin', facility=1)

        client_admin_2 = create_random_user(
            grit_admin_2, user='client_admin', facility=1)
        client_admin_2_id = client_admin_2['user_id']

        user_admin_2 = create_random_user(
            grit_admin_2, user='user_admin', facility=1)
        user_admin_2_id = user_admin_2['user_id']

        create_random_user(grit_admin_2, user='user', facility=1)

        # Request by a user that doesnt exist.
        self.client.set_cookie(url, 'user_id', self.fake_user_id)
        user = self.client.put(
            url, data=dict(
                user_id=grit_admin_1_id, role_id=4))
        self.assertEqual(user.status_code, 403)

        # Request to delete a user that doesnt exist.
        self.client.set_cookie(url, 'user_id', grit_admin_1_id)
        user = self.client.put(
            url,
            data=dict(
                user_id=self.fake_user_id,
                role_id=5))
        self.assertEqual(user.status_code, 404)

        # Request by a grit admin to change role of another grit admin.
        self.client.set_cookie(url, 'user_id', grit_admin_1_id)
        user = self.client.put(
            url, data=dict(
                user_id=grit_admin_2_id, role_id=4))
        self.assertEqual(user.status_code, 200)

        # ###############################  SAME FACILITIES ####################

        # Request by a lower role user to change role of a user with higher
        # role in same facility;
        self.client.set_cookie(url, 'user_id', user_admin_1_id)
        user = self.client.put(
            url,
            data=dict(
                user_id=client_admin_1_id,
                role_id=2))
        self.assertEqual(user.status_code, 403)

        # Request to change role of a user with same role in same facility;
        self.client.set_cookie(url, 'user_id', client_admin_1_id)
        user = self.client.put(
            url,
            data=dict(
                user_id=a_client_admin_1_id,
                role_id=2))
        self.assertEqual(user.status_code, 403)

        # Request to change role of a user with lower role in same facility to
        # higher role than theirs;
        self.client.set_cookie(url, 'user_id', client_admin_1_id)
        user = self.client.put(
            url, data=dict(
                user_id=user_admin_1_id, role_id=4))
        self.assertEqual(user.status_code, 403)

        # Request to change role of a user with lower role in same facility to
        # higher role but lower than theirs;
        self.client.set_cookie(url, 'user_id', mgt_admin_1_id)
        user = self.client.put(
            url, data=dict(
                user_id=user_admin_1_id, role_id=3))
        self.assertEqual(user.status_code, 200)

        #############################  DIFFERENT FACILITIES ###################

        # Request by a lower role user to change role of a user with higher
        # role in different facility;
        self.client.set_cookie(url, 'user_id', user_admin_1_id)
        user = self.client.put(
            url,
            data=dict(
                user_id=client_admin_2_id,
                role_id=1))
        self.assertEqual(user.status_code, 403)

        # Request to change role of a user with same role in different
        # facility;
        self.client.set_cookie(url, 'user_id', client_admin_1_id)
        user = self.client.put(
            url,
            data=dict(
                user_id=client_admin_2_id,
                role_id=1))
        self.assertEqual(user.status_code, 403)

        # Request to change role of a user with lower role in different
        # facility to higher role than theirs;
        self.client.set_cookie(url, 'user_id', client_admin_1_id)
        user = self.client.put(
            url, data=dict(
                user_id=user_admin_2_id, role_id=4))
        self.assertEqual(user.status_code, 403)

        # Request to change role of a user with lower role in different facility to a higher role but lower than theirs;
        # Works but messes with db
        # self.client.set_cookie(url, 'user_id', mgt_admin_1_id)
        # user = self.client.put(url, data=dict(user_id=user_admin_2_id, role_id=3))
        # self.assertEqual(user.status_code, 403)


if __name__ == "__main__":
    unittest.main()
