import os
from ast import literal_eval


class Config:
    DEBUG = False
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SQLALCHEMY_ECHO = False
    # SQLALCHEMY_POOL_SIZE = 10
    JWT_KEY = os.environ['JWT_KEY']
    SECRET_KEY = os.environ['SECRET_KEY']
    PASSWORD_STRENGTH = os.environ['PASSWORD_STRENGTH']
    AUTHY_API_KEY = os.environ['AUTHY_API_KEY']
    ADMINS = ['workshop@grit.systems', ]
    INVITE_EXPIRES = os.environ['INVITE_EXPIRES']  # in seconds.
    MAIL_SERVER = 'secure46.webhostinghub.com'
    MAIL_PORT = 465
    MAIL_USE_TLS = False
    MAIL_USE_SSL = True
    MAIL_USERNAME = 'gtg@grit.systems'
    MAIL_PASSWORD = '01*CqgBD@Oo2*T'
    SIGNUP_PAGE = os.environ['SIGNUP_PAGE']
    HOME_PAGE = 'http://gtg.grit.systems:5600/'
    HOST_URL = os.environ['HOST_URL']
    USERS_PER_PAGE = 20
    BOOTSTRAP_SERVERS = os.environ['BOOTSTRAP_SERVERS']
    SCHEMA_REGISTRY_URI = os.environ['SCHEMA_REGISTRY_URI']
    TOPICS = literal_eval(os.environ['TOPICS'])
    SCHEMA_MAPPING = literal_eval(os.environ['SCHEMA_MAPPING'])
    TOPIC_SCHEMA_MAPPING = dict(zip(TOPICS, SCHEMA_MAPPING))
    BLOCKCHAIN_API_URI = os.environ['BLOCKCHAIN_API_URI']

    @classmethod
    def init_app(cls, app):
        pass
        # from api.producer import KafkaProducer
        # # app.kafka_producer = KafkaProducer(
        # # 	bootstrap_servers=cls.BOOTSTRAP_SERVERS,
        # # 	schema_registry_uri=cls.SCHEMA_REGISTRY_URI,
        # # 	topic_schema_mapping=cls.TOPIC_SCHEMA_MAPPING,
        # # )


class Development(Config):
    DEBUG = True
    SQLALCHEMY_DATABASE_URI = os.environ['DATABASE_URL']
    # PASSWORD_STRENGTH = os.environ.get('PASSWORD_STRENGTH')
    # PASSWORD_STRENGTH = 5
    # JWT_SECRET_KEY = os.urandom(3)

    def __repr__(self):
        return '{}'.format(self.__class__.__name__)


class Testing(Config):
    """TODO"""
    TESTING = True
    # SQLALCHEMY_DATABASE_URI = 'sqlite://'
    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL')
    SECRET_KEY = b'-\x85RJ{K\xf4x\x0f\x82\xa0U\x06\xcf\xbb\xa3\xb89v!\xeeO\x8cX'
    PASSWORD_STRENGTH = '5'
    AUTHY_API_KEY = 'jNXWyAEkxKKvJwBvCpvXJDHLFnYPq9Im'
    USERS_PER_PAGE = 20

    def __repr__(self):
        return '{}'.format(self.__class__.__name__)


class Staging(Config):
    """TODO"""
    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL')
    JWT_KEY = os.environ['JWT_KEY']
    SECRET_KEY = os.environ['SECRET_KEY']
    PASSWORD_STRENGTH = os.environ['PASSWORD_STRENGTH']
    AUTHY_API_KEY = os.environ['AUTHY_API_KEY']

    def __repr__(self):
        return '{}'.format(self.__class__.__name__)


class Production(Config):
    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL')
    PASSWORD_STRENGTH = os.environ.get('PASSWORD_STRENGTH')
    RESTPLUS_MASK_SWAGGER = True

    def __repr__(self):
        return '{}'.format(self.__class__.__name__)


config = {
    'Development': Development,
    'Testing': Testing,
    'Production': Production,
    'Staging': Staging,

    'default': Development,


}
