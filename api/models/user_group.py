import time

import requests
from flask import current_app as app

from api import db
from api.models.base import Base
from logger import logger


class UserGroup(Base):
    id = db.Column(db.Integer, autoincrement=True,
                   unique=True, primary_key=True)
    group_id = db.Column(db.String, unique=True)
    users = db.relationship('Users', backref='group', lazy='dynamic')
    # prosumers = db.relationship(
    #     'Prosumer', backref='user_group', lazy='dynamic', cascade="all, save-update")
    blockchain_address = db.Column(db.String, default="")
    blockchain_private_key = db.Column(db.String, default="")

    def __init__(self, group_id):
        """Init end consumer"""
        self.group_id = group_id

    def __repr__(self):
        return 'UserGroup = {}'.format(self.group_id)

    @staticmethod
    def get_group(group_id=None, blockchain_address=None):
        """
                Method to get a group during registration and group login
        """
        if group_id:
            group = UserGroup.query.filter_by(group_id=group_id).first()
            return group
        group = UserGroup.query.filter_by(
            blockchain_address=blockchain_address).first()
        return group

    def edit_group(self, blockchain_address, blockchain_private_key):
        if blockchain_address and blockchain_address:
            self.blockchain_address = blockchain_address
            self.blockchain_private_key = blockchain_private_key
            db.session.add(self)
            db.session.commit()
            db.session.refresh(self)
            return self.to_dict()
        return dict()

    @staticmethod
    def get_all():
        all_groups = UserGroup.query.all()
        group_count = len(all_groups)
        if group_count > 0:
            json_groups = [group.to_dict() for group in all_groups]
        else:
            json_groups = ['No groups.']
        return json_groups

    def members(self):
        """
                Method to get a group during registration and group login
        """
        users = self.users.all()
        group_members = []
        if users:
            group_members = [
                UserGroup.parse_user_dict(
                    user.to_dict()) for user in users]
        return group_members

    def get_group_admin(self):
        admin = None
        grit_admins = []
        users = self.users.all()
        if not users:
            return self
        for user in users:
            if user.is_a('grit_admin'):
                grit_admins.append(user)
                continue
            if not admin:
                admin = user
            else:
                if admin.role_id == user.role_id:
                    if admin.date_created < user.date_created:
                        admin = user
        if not admin:
            if grit_admins:
                current_admin = None
                for grit_admin in grit_admins:
                    if grit_admin.date_created < current_admin.date_created:
                        grit_admin = current_admin
                admin = current_admin
        return admin

    @staticmethod
    def parse_user_email(user_dict):
        params = {}
        params['email'] = user_dict['email']
        params['phone_number'] = user_dict['phone_number']
        params['group_id'] = user_dict['group_id']
        return params

    @staticmethod
    def parse_user_dict(user_dict):
        del user_dict['group_id']
        return user_dict

    def to_dict(self):
        members = self.members()
        group_admin = self.get_group_admin()
        json_group = {
            'group_id': self.group_id,
            'group_admin': group_admin.to_dict() if group_admin else group_admin,
            'members': members,
            'number_of_members': len(members),
            'urls': {
                'prosumers': 'prosumers/user/{}'.format(self.group_id),
            },
            'blockchain_account': {
                'address': self.blockchain_address,
                'private_key': self.blockchain_private_key,
            },
        }
        return json_group


@db.event.listens_for(db.session, 'before_flush')
def new_user_listner(session, flush_context, instance):
    session.changes = {
        'add': list(session.new),
        'update': list(session.dirty),
        'delete': list(session.deleted)
    }
    for obj in session.changes['add']:
        if isinstance(obj, UserGroup):
            attach_address(obj, action="saved")
    for obj in session.changes['update']:
        if isinstance(obj, UserGroup):
            attach_address(obj, action='updated')
    for obj in session.changes['delete']:
        if isinstance(obj, UserGroup):
            print('UserGroup to be deleted')


def attach_address(user_group, action):
    print(f"User: {user_group} is to be {action} ")
    if user_group.blockchain_address == "" and user_group.blockchain_private_key == "":
        time.sleep(0.5)
        print("Attach address")
        # todo move to config.py
        blockchain_new_account_url = f"{app.config['BLOCKCHAIN_API_URI']}/new-account"
        new_account_response = requests.get(url=blockchain_new_account_url)
        if new_account_response.status_code == 200:
            new_account_data = new_account_response.json()
            try:
                address = new_account_data['data']['address']
                private_key = new_account_data['data']['private_key']
            except KeyError:
                return False
            print(address, private_key)
            user_group.blockchain_private_key = private_key
            user_group.blockchain_address = address
            db.session.add(user_group)
            print(user_group)

            logger.info(
                f"Call to {blockchain_new_account_url}: {new_account_data['success']}, message: {new_account_data['message']}")
        # todo one can not be "" while the other is not
            return True
    return False


# db.event.listen(db.session, 'before_commit', new_user_listner)
# db.event.listen(db.session, 'after_commit', after_commit_user)
# todo attach blockchain params async. through kafka, using the after flush event,
# since the db is not changed it is okay.
