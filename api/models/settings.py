from api import db
from api.models.base import Base


class Settings(Base):
    id = db.Column(db.Integer, autoincrement=True,
                   unique=True, primary_key=True)
    use_mfa = db.Column(db.Boolean, default=False)
    gtg_user = db.Column(db.Boolean, default=False)
    disco_user = db.Column(db.Boolean, default=False)
    configuration_user = db.Column(db.Boolean, default=False)
    use_keypad = db.Column(db.Boolean, default=False)
    is_migrated = db.Column(db.Boolean, default=False)
    users = db.relationship("Users", backref='settings', uselist=False)
