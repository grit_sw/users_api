from api import db


class Permission:
    DEMO = 1
    DEFAULT = 2
    CREATE_BUYORDER = 4
    CREATE_SELLORDER = 8
    BUY_ELECTRICITY = 16

    CREATE_PROSUMER = 32
    CREATE_USER = 64
    CREATE_USER_ADMIN = 128

    CREATE_CLIENT_ADMIN = 256
    CREATE_PROSUMER_GROUP = 512
    CREATE_MANAGEMENT_ADMIN = 1024

    @staticmethod
    def perm_params(permissions):
        perms = Permission.__dict__.items()
        if perms >= 1:
            _data = [perm for perm in perms
                     if not perm[0].endswith('__')]
            data = [p for p in _data if p < permissions]
        else:
            data = 'No Permissions found'
        return data

    @staticmethod
    def to_dict(permissions):
        data = Permission.perm_params(permissions)
        return {'data': data}


class Role(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64), unique=True)
    default = db.Column(db.Boolean, default=False, index=True)
    permissions = db.Column(db.Integer)
    users = db.relationship('Users', backref='role', lazy='dynamic')

    def __init__(self, **kwargs):
        super(Role, self).__init__(**kwargs)
        if self.permissions is None:
            self.permissions = 0

    @staticmethod
    def insert_roles():
        roles = {'guest': (0,
                           [Permission.DEMO]),
                 'user': (1,
                          [Permission.DEFAULT,
                           Permission.CREATE_BUYORDER,
                           Permission.BUY_ELECTRICITY]),
                 'user_admin': (2,
                                [Permission.DEFAULT,
                                 Permission.CREATE_BUYORDER,
                                 Permission.BUY_ELECTRICITY,
                                 Permission.CREATE_USER]),
                 'client_admin': (3,
                                  [Permission.DEFAULT,
                                   Permission.CREATE_BUYORDER,
                                   Permission.CREATE_PROSUMER,
                                   Permission.CREATE_USER,
                                   Permission.CREATE_USER_ADMIN]),
                 'management_admin': (4,
                                      [Permission.DEFAULT,
                                       Permission.CREATE_BUYORDER,
                                       Permission.CREATE_SELLORDER,
                                       Permission.BUY_ELECTRICITY,
                                       Permission.CREATE_PROSUMER,
                                       Permission.CREATE_USER_ADMIN,
                                       Permission.CREATE_CLIENT_ADMIN,
                                       Permission.CREATE_PROSUMER_GROUP]),
                 'grit_admin': (5,
                                Role.all_perms()),
                 }
        default_role = 'guest'

        for r in roles:
            print(r)
            role = Role.query.filter_by(name=r).first()
            if role is None:
                role = Role(name=r)
            role.reset_permissions()
            for perm in roles[r][1]:
                role.add_permission(perm)
            role.default = (role.name == default_role)
            role.id = roles[r][0]
            db.session.add(role)
        db.session.commit()
        db.session.close()

    def add_permission(self, perm):
        if not self.has_permission(perm):
            self.permissions += perm

    def remove_permission(self, perm):
        if self.has_permission(perm):
            self.permissions -= perm

    def reset_permissions(self):
        self.permissions = 0

    def has_permission(self, perm):
        return self.permissions & perm == perm

    @staticmethod
    def all_perms():
        permissions = Permission.__dict__.items()
        # Dont throw the R1717 warning for the next line of code
        # pylint: disable=R1717
        g_admin_perms = dict([attr for attr in permissions if not attr[0].startswith(
            "__") if isinstance(attr[1], int)]).values()
        # pylint: enable=R1717
        return g_admin_perms

    @staticmethod
    def get_role(name):
        role = Role.query.filter_by(name=name).first()
        return role

    @staticmethod
    def all_roles():
        roles = Role.query.all()
        role_count = len(roles)
        if role_count >= 1:
            json_roles = [role.to_dict() for role in roles]
        else:
            json_roles = ['Roles not assigned']
        return role_count, json_roles

    def to_dict(self):
        json_role = {
            'id': str(self.id),
            'name': self.name.replace('_', ' ').title(),
        }
        return json_role

    def __repr__(self):
        return '<Role %r>' % self.name
