
import os


from pymongo import MongoClient

from api import db as sqldb
from api.models import Users
from logger import logger

client = MongoClient(os.getenv('MONGO_DB_URL'))
db = client['lift-mongo-app']
configurations = db['configurations']
probes = db['probes']
powersources = db['powersources']
users = db['user.users']

user_list = [
    'gbenga71', 'yoda', 'viathan', 'M_OKUEYUNGBO', 'vanpeux',
    'GRIT', 'Rensource', 'tobi', 'ikenna', 'idarlington83', 'Femi',
    'Afam Ochei', 'GREEN_LIFE_PHARM', 'sbe2', 'PowerCell'
]


def user_generator(current_user_list):
    for user in current_user_list:
        yield user


def get_user(user_name):
    return users.find_one({'username': user_name})


def get_user_params(user_dict):
    return user_params(user_dict)


def get_role(dashboard_role):
    roles = {
        'guest': 'guest',
        'user': 'user_admin',
        'regionaladmin': 'management_admin',
        'superuser': 'management_admin',
        'admin': 'grit_admin',
    }
    return roles[dashboard_role]


def user_params(user_dict):
    try:
        user_role = user_dict['roles'][0]
        role_ = get_role(user_role)
    except IndexError:
        logger.info(user_dict)
        return None

    name = user_dict['username'].replace('_', ' ').split(' ')
    if len(name) == 2:
        first, last = name
    if len(name) == 3:
        first, second, last = name
        first = f'{first} {second}'
    else:
        first, last = name[0], None
    response = {
        'user_id': str(user_dict['_id']),
        'group_id': str(user_dict['_id']),
        'first_name': first,
        'last_name': last,
        'role_name': role_,
        'email': user_dict['email'],
        'password_hash': user_dict['password'].encode('utf-8'),
        'is_migrated': True,
        'configuration_user': True
    }
    return response


def create_user(user):
    user = Users.migrate(**user)
    return user


def drive():
    for user_name in user_generator(user_list):
        user_dict = get_user(user_name)
        user = get_user_params(user_dict)
        if not user:
            continue
        created_user = create_user(user)
        print(created_user)
    sqldb.session.commit()
