from time import sleep, time
from uuid import uuid4

import arrow
import jwt
from sqlalchemy import or_
from flask import current_app as app

from api import db
from api.models.base import Base
from api.models.setup_mixins import UserSetupMixin
from api.models import models
from api.models.roles import Role
from api.models.user_group import UserGroup
from api.models.settings import Settings
from api.utils import get_authy_client, send_authy_token_request, verify_authy_token, create_authy_user, delete_authy_user
from api.exceptions import UUIDClashError, UnableToDeleteError
from api import emails
from logger import logger


user_facility = db.Table(
    'facilities',
    db.Column('user_id', db.Integer, db.ForeignKey('users.id')),
    db.Column('facility_id', db.Integer, db.ForeignKey('facility.id')),
    # db.UniqueConstraint('user_id', 'facility_id', name='user_id_facility_id_unique_pairs'),
)


class Users(Base, UserSetupMixin):
    AUTHY_STATUSES = (
        'unverified',
        'onetouch',
        'sms',
        'token',
        'approved',
        'denied'
    )
    id = db.Column(db.Integer, autoincrement=True,
                   unique=True, primary_key=True)
    first_name = db.Column(db.String(128))
    last_name = db.Column(db.String(128))
    user_id = db.Column(db.String, unique=True)
    email = db.Column(db.String(75))
    password_hash = db.Column(db.Binary(192))
    phone_number = db.Column(db.String(20))
    invite_sent = db.Column(db.Boolean)
    invite_sent_date = db.Column(db.DateTime)

    invite_clicked = db.Column(db.DateTime)
    signed_up = db.Column(db.Boolean)
    last_seen = db.Column(db.DateTime)
    is_active = db.Column(db.Boolean)
    mfa_sent = db.Column(db.Boolean)
    mfa_validated = db.Column(db.Boolean)
    mfa_code = db.Column(db.String(10))

    device_id = db.Column(db.String(50))
    customer_id = db.Column(db.String(50))

    role_id = db.Column(db.Integer, db.ForeignKey('role.id'))
    parent_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    group_id = db.Column(db.Integer, db.ForeignKey('user_group.id'))
    settings_id = db.Column(db.Integer, db.ForeignKey('settings.id'))
    facility = db.relationship(
        'Facility',
        secondary=user_facility,
        backref=db.backref(
            'users',
            lazy='dynamic'))

    parent = db.relationship("Users", remote_side=[id], backref='children')
    authy_id = db.Column(db.Integer)
    authy_status = db.Column(db.Enum(*AUTHY_STATUSES, name='authy_statuses'))
    country_code = db.Column(db.String, default='234')

    def __repr__(self):
        return '<User {}>'.format(self.email or self.phone_number)

    def __init__(
            self,
            email,
            role_id=None,
            facility_id=None,
            parent_id=None,
            use_mfa=False,
            gtg_user=False,
            disco_user=False,
            configuration_user=False,
            use_keypad=False,
            is_active=False,
            set_up=True):
        """Init end consumer"""
        self.email = self.set_email(email)
        if set_up:
            if parent_id is not None:
                parent = Users.get_user(user_id=parent_id)
            role = Role.query.filter_by(id=role_id).first()
            my_facility = None
            if facility_id is not None:
                my_facility = models.Facility.get_facility(
                    facility_id=facility_id)
            self.user_id = str(uuid4())
            self.is_active = is_active
            self.role = role
            if parent_id is not None:
                self.parent = parent
            if my_facility is not None:
                self.facility.append(my_facility)
            db.session.add(self)
            self.settings = Settings(
                use_mfa=use_mfa,
                gtg_user=gtg_user,
                configuration_user=configuration_user,
                use_keypad=use_keypad,
                disco_user=disco_user)
            if self.can_create_users():
                group_id = str(uuid4())
                new_group = UserGroup(group_id)
                new_group.users.append(self)

            else:
                # To automatically add new users to the UserAdmin group
                parent_group = self.parent.group
                parent_group.users.append(self)
                db.session.add(parent_group)

    @staticmethod
    def migrate(email, role_name, user_id, group_id, first_name, last_name,
                password_hash, is_migrated, configuration_user):
        user = Users.get_user(user_id=user_id)
        if user:
            return user
        role = Role.query.filter_by(name=role_name).first()
        user = Users(email, set_up=False)
        user.first_name = user.set_name(first_name)
        if last_name:
            user.last_name = user.set_name(last_name)
        user.password_hash = password_hash
        user.user_id = user_id
        user.signed_up = True
        user.role = role
        user_group = UserGroup(group_id)
        user_group.users.append(user)
        user.settings = Settings(
            is_migrated=is_migrated,
            configuration_user=configuration_user)
        db.session.add(user)
        return user

    @property
    def full_name(self):
        if not self.first_name:
            return f'{self.last_name}'
        if not self.last_name:
            return f'{self.first_name}'
        return f'{self.first_name} {self.last_name}'

    @staticmethod
    def search_for(search_term, size, facility_id=None, search_role=None):
        """Method that searches for a user by email, first name, last name or phone number"""
        try:
            users = Users.query.filter(or_(
                Users.phone_number.like('%' + search_term + '%')
            )
            )
        except ValueError:
            users = Users.query.filter(or_(
                Users.first_name.like('%' + search_term.title() + '%'),
                Users.last_name.like('%' + search_term.title() + '%'),
                Users.email.like('%' + search_term + '%')
            )
            )
        if facility_id:
            users = users.filter_by(
                role_id=search_role).order_by(
                Users.first_name).join(
                Users.facility).filter_by(
                facility_id=str(facility_id)).paginate(
                    1,
                    size,
                False)
        else:
            users = users.order_by(Users.first_name).paginate(1, size, False)
        all_users = [user.to_dict() for user in users.items]
        return all_users, users.has_prev, users.has_next

    @staticmethod
    def new_user(payload):
        """Method that saves a new user to the database"""
        failed_invites = []
        successful_emails = 0
        facility_id = payload['facility_id']
        role_id = int(payload['role_id'])
        use_mfa = payload['mfa_required']
        gtg_user = payload['gtg_user']
        disco_user = payload.get('disco_user')
        configuration_user = payload['configuration_user']
        use_keypad = payload['keypad_user']
        parent_id = payload['user_id']
        invite_emails = (invite[0] for invite in payload['emails'])
        for email in invite_emails:
            user = Users.get_user(email=email)
            if user:
                if not bool(user.signed_up):
                    sender = Users.get_user(user_id=parent_id)
                    receiver = user
                    emails.send_invite_email(receiver, sender=sender)
                    continue
                continue  # don't recreate a user if the user already exists.
            try:
                str(uuid4())
                str(uuid4())
            except UUIDClashError as e:
                logger.exception(e)
                failed_invites.append(email)
                continue

            logger.info(f'Creating <NewUser {email}>')
            new_user = Users(
                email=email.lower(),
                role_id=role_id,
                facility_id=facility_id,
                use_mfa=use_mfa,
                gtg_user=gtg_user,
                disco_user=disco_user,
                configuration_user=configuration_user,
                use_keypad=use_keypad,
                parent_id=parent_id,
            )
            db.session.add(new_user)
            db.session.flush()
            successful_emails += 1
            # Make request to auth api for token here
            sender = Users.get_user(user_id=parent_id)
            receiver = Users.get_user(email=email)
            emails.send_invite_email(receiver, sender=sender)
            logger.info(f'Added < User with email {email} >')
        try:
            db.session.commit()
        except Exception as e:
            logger.exception(e)
            logger.info('Payload {} failed to save properly'.format(payload))
            db.session.rollback()
        return failed_invites, successful_emails

    def create_invite_token(self, expires_in):
        claims = {
            'exp': time() + expires_in,
            'sub': {
                'user_id': self.user_id,
                'group_id': self.get_group_id,
                "role_id": self.get_role_id,
                "email": self.email,
                "facilities": self.facilities,
            }
        }
        return jwt.encode(
            claims,
            app.config['JWT_KEY'], algorithm='HS256').decode('utf-8')

    @staticmethod
    def check_invite_token(token):
        try:
            payload = jwt.decode(
                token,
                app.config['JWT_KEY']
            )
            return payload['sub']['user_id'], True
        except Exception:
            return 'Invalid token.', False

    def create_user(
            self,
            first_name,
            last_name,
            email,
            password,
            phone_number):
        """Method that saves the a new user to the database"""
        created = False
        if self.settings.use_mfa:
            authy_mobile = self.authy_mobile(phone_number)
            logger.info(
                'Registering User {} to Authy Service.'.format(authy_mobile))
            client = get_authy_client()
            authy_user = client.users.create(
                email, authy_mobile, self.country_code)

            if authy_user.ok():
                logger.info(
                    'User {} registered to Authy Service'.format(phone_number))
                self.first_name = self.set_name(first_name)
                self.last_name = self.set_name(last_name)
                # self.email = self.set_email(email)
                self.password_hash = self.set_password(password)
                self.phone_number = self.set_phone_number(phone_number)
                self.authy_id = authy_user.id
                self.signed_up = True
                self.is_active = True
                db.session.add(self)
                try:
                    db.session.commit()
                    # 1. Make request to auth api for token here
                    # 2. Send mfa_code pin here
                    db.session.refresh(self)
                    created = True
                    logger.info(
                        'Created <User {} with UUID {} and email {}>'.format(
                            phone_number, self.user_id, self.email))
                except Exception as e:
                    logger.exception(e)
                    db.session.rollback()
                    return created
            else:
                logger.info(authy_user)
                logger.info(authy_user.response)
                logger.info(dir(authy_user))
                logger.warning(
                    'FAILED CREATION OF Authy profile for<User {} with email {}>'.format(
                        self.full_name, email))
                return created
        else:
            logger.info(
                'User {} not registered to Authy Service'.format(phone_number))
            self.first_name = self.set_name(first_name)
            self.last_name = self.set_name(last_name)
            self.email = self.set_email(email)
            self.password_hash = self.set_password(password)
            self.phone_number = self.set_phone_number(phone_number)
            self.signed_up = True
            self.is_active = True
            logger.info('User {} saved to the database'.format(
                self.phone_number))
            db.session.add(self)
            try:
                db.session.commit()
                db.session.refresh(self)
                created = True
                logger.info('Created <User {} with UUID {}>'.format(
                    phone_number, self.user_id))
            except Exception as e:
                logger.exception(e)
                db.session.rollback()
                return created
        return created

    def edit_me(self, first_name, last_name, email, phone_number, use_mfa):
        self.first_name = self.set_name(first_name)
        self.last_name = self.set_name(last_name)
        self.email = self.set_email(email)
        self.phone_number = self.set_phone_number(phone_number)
        self.settings.use_mfa = use_mfa
        db.session.add(self)
        db.session.commit()
        db.session.refresh(self)
        return self.to_dict()

    def edit_user(
            self,
            first_name,
            last_name,
            email,
            phone_number,
            gtg_user,
            configuration_user,
            use_mfa,
            use_keypad):
        self.first_name = self.set_name(first_name)
        self.last_name = self.set_name(last_name)
        self.email = self.set_email(email)
        self.phone_number = self.set_phone_number(phone_number)
        self.settings.use_mfa = use_mfa
        self.settings.gtg_user = gtg_user
        self.settings.configuration_user = configuration_user
        self.settings.use_keypad = use_keypad
        db.session.add(self)
        db.session.commit()
        db.session.refresh(self)
        return self.to_dict()

    @staticmethod
    def set_invite_clicked(user):
        user.invite_clicked = arrow.now().datetime
        db.session.add(user)
        logger.info("User {} clicked signup link on {}".format(
            user.user_id, user.invite_clicked))
        try:
            db.session.commit()
            db.session.refresh(user)
        except Exception as e:
            logger.exception(e)
            db.session.rollback()

    def invite_status(self):
        status = {}
        status['invite_sent'] = self.invite_sent
        status['invite_sent_date'] = str(self.invite_sent_date)
        status['invite_clicked'] = bool(self.invite_clicked)
        status['invite_clicked_date'] = str(self.invite_clicked)
        status['user_signed_up'] = self.signed_up
        return status

    def resend_invite(self):
        emails.send_invite_email(self)

    @staticmethod
    def new_user_id():
        count = 0
        while True:
            if count >= 20:
                raise UUIDClashError(
                    'User UUID generation failed after {} attempts'.format(count))

            count += 1
            user_id = str(uuid4())
            user = Users.get_user(user_id=user_id)
            if not user:
                break
            logger.warning(
                'UUID generation clash with User {}'.format(user.user_id))
            sleep(0.01)
        return user_id

    @staticmethod
    def new_group_id():
        count = 0
        while True:
            if count >= 20:
                raise UUIDClashError(
                    'User UUID generation failed after {} attempts'.format(count))

            count += 1
            group_id = str(uuid4())
            group = UserGroup.get_group(group_id=group_id)
            if not group:
                break
            logger.warning(
                'UUID generation clash with GROUP {}'.format(group.group_id))
            sleep(0.01)
        return group_id

    @staticmethod
    def get_user(user_id=None, email=None, phone_number=None):
        """
                Method to get a user during registration and user login
        """
        if user_id:
            user = Users.query.filter_by(user_id=user_id).first()
            return user
        if email:
            user = Users.query.filter_by(email=email.lower()).first()
            return user
        try:
            phone_number = UserSetupMixin().set_phone_number(phone_number)
            user = Users.query.filter_by(phone_number=phone_number).first()
            return user
        except Exception as e:
            logger.exception(e)

    @staticmethod
    def delete_user(user_id):
        users = Users.query.filter_by(user_id=user_id).all()
        deleted = False

        if users:
            for user in users:
                if user.authy_id:
                    try:
                        logger.info(
                            "Deleting user {} from authy".format(
                                user.email))
                        delete_authy_user(user.authy_id)
                        logger.info(
                            "Deleted user {} from authy".format(
                                user.email))
                    except Exception as e:
                        logger.critical(e)
                        return deleted

                try:
                    db.session.delete(user)
                    db.session.commit()
                    deleted = True
                except Exception as e:
                    logger.exception(e)
                    db.session.rollback()
                    raise UnableToDeleteError('Unable to delete')

        return deleted

    def can_create_users(self):
        result = self.is_a('grit_admin') or self.is_a('mgt_admin') \
            or self.is_a('client_admin') or \
            self.is_a('user_admin')
        return result

    def can(self, perm):
        return self.role is not None and self.role.has_permission(perm)

    @staticmethod
    def users_by_role(page, user_id, role_id):
        """
                Method to view all the users with a particular role in ALL facilities.
                Only available to the GRIT admins
        """
        print(user_id)
        users = Users.query.filter_by(
            role_id=role_id).paginate(
            page, app.config['USERS_PER_PAGE'], False)
        if not users.items:
            return [], False, False

        all_users = [user.to_dict() for user in users.items]
        return all_users, users.has_prev, users.has_next

    @staticmethod
    def all_users(page, user_id):
        print(user_id)
        users = Users.query.paginate(
            page, app.config['USERS_PER_PAGE'], False)

        if not users.items:
            return [], False, False
        all_users = [user.to_dict() for user in users.items]
        return all_users, users.has_prev, users.has_next

    @staticmethod
    def all_config_users(page):
        users = Users.query.join(Settings).filter_by(configuration_user=True).paginate(
            page, app.config['USERS_PER_PAGE'], False)

        if not users.items:
            return [], False, False
        all_users = [user.to_dict() for user in users.items]
        return all_users, users.has_prev, users.has_next

    @staticmethod
    def all_users_in_a_facility(page, user_id, facility_id):
        requester = Users.get_user(user_id=user_id)
        r_role = requester.role_id

        if requester.is_a('grit_admin'):
            users = Users.query.filter(
                Users.role_id <= r_role).join(
                Users.facility).filter_by(
                facility_id=facility_id).paginate(
                page, app.config['USERS_PER_PAGE'], False)

            if not users.items:
                return [], False, False
            all_users = [user.to_dict() for user in users.items]
            return all_users, users.has_prev, users.has_next

        requester_facilities = {facility['facility_id'] for facility in requester.facilities}

        if facility_id not in requester_facilities:
            return [], False, False

        users = Users.query.filter(
            Users.role_id <= r_role).join(
            Users.facility).filter_by(
            facility_id=facility_id).paginate(
            page, app.config['USERS_PER_PAGE'], False)
        if not users.items:
            return [], False, False
        all_users = [user.to_dict() for user in users.items]
        return all_users, users.has_prev, users.has_next

    def is_child_role(self, role_id):
        role_id = int(role_id)
        child_ids = {int(role['id']) for role in self.child_roles()}
        return bool(role_id in child_ids)

    @staticmethod
    def all_users_in_all_facilities_with_role(page, role_id):
        users = Users.query.filter_by(
            role_id=role_id).paginate(
            page, app.config['USERS_PER_PAGE'], False)
        if not users.items:
            return [], False, False
        return [user.to_dict()
                for user in users.items], users.has_prev, users.has_next

    @staticmethod
    def all_users_in_one_facility_with_role(page, role_id, facility_id):
        users = Users.query.filter_by(
            role_id=role_id).join(
            Users.facility,
            aliased=True).filter_by(
            facility_id=facility_id).paginate(
                page,
                app.config['USERS_PER_PAGE'],
            False)
        if not users.items:
            return [], False, False
        return [user.to_dict()
                for user in users.items], users.has_prev, users.has_next

    @staticmethod
    def all_users_in_a_group(page, group_id):
        users = Users.query.join(
            Users.group).filter_by(
            group_id=group_id).paginate(
            page, app.config['USERS_PER_PAGE'], False)
        if not users.items:
            return [], False, False
        return [user.to_dict()
                for user in users.items], users.has_prev, users.has_next

    @staticmethod
    def max_admin_in_group(group_id):
        max_admin = Users.query.join(
            Users.group).filter_by(
            group_id=group_id).order_by(
            Users.role_id.desc()).first()
        if not max_admin:
            return {}
        return max_admin.to_dict()

    @staticmethod
    def facility_users(facility_id, page):
        users = Users.query.join(
            Users.facility,
            aliased=True).filter_by(
            facility_id=facility_id).paginate(
            page,
            app.config['USERS_PER_PAGE'],
            False).items
        if not users:
            _users = []
        else:
            _users = [facility.to_dict() for facility in users]
        return _users

    @property
    def has_children(self):
        return bool(self.children)

    def is_ancestor_of(self, user):
        higher_role = self.role_id > user.role_id
        facilities_1 = self.my_facility_ids()
        facilities_2 = user.my_facility_ids()
        same_facility = any(id for id in facilities_1 if id in facilities_2)
        if higher_role and same_facility:
            return True
        return False

    def is_descendant_of(self, user):
        lower_role = self.role_id < user.role_id
        facilities_1 = self.my_facility_ids()
        facilities_2 = user.my_facility_ids()
        same_facility = any(id for id in facilities_1 if id in facilities_2)
        if lower_role and same_facility:
            return True
        return False

    def is_sibling_of(self, user):
        same_role = self.role_id == user.role_id
        facilities_1 = self.my_facility_ids()
        facilities_2 = user.my_facility_ids()
        same_facility = any(id for id in facilities_1 if id in facilities_2)
        if same_role and same_facility:
            return True
        return False

    def in_same_facility(self, user):
        facilities_1 = self.my_facility_ids()
        facilities_2 = user.my_facility_ids()
        same_facility = any(id for id in facilities_1 if id in facilities_2)
        if same_facility:
            return True
        return False

    def is_a(self, role_name):
        if self.role:
            return self.role.name == role_name
        return False

    @staticmethod
    def get_customer(parent_id, customer_id):
        """Method to get a customer ID after registration"""
        client_admin = Users.get_client_admin(parent_id)
        customer = client_admin.query.filter_by(
            customer_id=customer_id).first()
        return customer

    @staticmethod
    def get_user_group(group_id):
        """
                Method to get a group during registration and group login
        """
        group = Users.query.join(
                Users.group).filter_by(
                group_id=group_id).first()
        return group

    @property
    def get_group_id(self):
        group_id = None
        if self.group is not None:
            group_id = self.group.group_id
        return group_id

    @property
    def get_role_id(self):
        role_id = None
        if self.role is not None:
            role_id = self.role.id
        return role_id

    @property
    def get_role_name(self):
        role_name = None
        if self.role is not None:
            role_name = self.role.name.replace('_', ' ').title()
        return role_name

    @property
    def facilities(self):
        # if self.role_id >= 5:
        # 	return models.Facility.all()
        facilities = models.Facility.my_facilities(self.user_id)
        return facilities

    def my_facility_ids(self):
        facility_ids = [facility['facility_id']
                        for facility in self.facilities]
        return facility_ids

    @staticmethod
    def get_facility(facility_id):
        facility = Users.query.join(
            Users.facility).filter_by(
            facility_id=facility_id).first()
        return facility

    @property
    def get_parent_email(self):
        parent_email = None
        if self.parent is not None:
            parent_email = self.parent.email
        return parent_email

    @property
    def member_since(self):
        return str(arrow.get(self.date_created))

    @property
    def invite_dict(self):
        params = {
            'roles': self.child_roles(),
            'facility': models.Facility.invite(user_id=self.user_id),
        }
        return params

    def to_dict(self):
        json_user = {
            'id': self.id,
            'first_name': self.first_name,
            'last_name': self.last_name,
            'full_name': self.full_name,
            'email': self.email,
            'user_id': self.user_id,
            'authy_id': self.authy_id,
            'mfa_user': self.settings.use_mfa,
            'gtg_user': self.settings.gtg_user,
            'disco_user': int(self.settings.disco_user),
            'configuration_user': self.settings.configuration_user,
            'migrated_user': self.settings.is_migrated,
            'keypad_user': self.settings.use_keypad,
            'phone_number': self.phone_number,
            'device_id': self.device_id,
            'customer_id': self.customer_id,
            'group_id': self.get_group_id,
            'member_since': self.member_since,
            'user_active': self.is_active,
            'signed_up': self.signed_up,
            'invite_clicked': str(self.invite_clicked),
            'user_creator': self.get_parent_email,
            'facilities': self.facilities,
            'role_name': self.get_role_name,
            'role_id': self.get_role_id,
            'urls': {
                'user': '/users/id/{}/'.format(self.user_id),
                'prosumers': '/prosumers/user/{}/'.format(self.get_group_id),
            },
            'last_seen': 'Coming Soon ...',
        }
        return json_user

    def child_role(self):
        current = self.role.permissions
        all_roles = Role.query.all()
        permissions = [role.permissions for role in all_roles]
        child_perm = max(x for x in permissions if x < current)
        child_role = Role.query.filter_by(permissions=child_perm).first()
        return child_role

    def child_roles(self):
        current = self.role.permissions
        all_roles = Role.query.all()
        if self.is_a('grit_admin'):
            roles = [role.to_dict() for role in all_roles]
            return roles
        permissions = [role.permissions for role in all_roles]
        child_perm = max(x for x in permissions if x < current)
        child_roles = Role.query.filter(Role.permissions <= child_perm).all()
        roles = [role.to_dict() for role in child_roles]
        return roles

    def register_on_authy(self):
        _authy_mobile = self.phone_number.replace("+234", "0")
        authy_mobile = self.authy_mobile(_authy_mobile)
        logger.info(
            'Registering User {} to Authy Service.'.format(authy_mobile)
        )
        authy_user = create_authy_user(
            self.email,
            authy_mobile,
            self.country_code
        )
        print("\n\n\n authy_user.content ", authy_user.content)
        return authy_user

    def enable_mfa(self):
        if self.authy_id:
            self.settings.use_mfa = True
            try:
                db.session.add(self)
                db.session.commit()
                return True
            except Exception as e:
                logger.exception(e)
                db.session.rollback()
                return False
        authy_user = self.register_on_authy()
        print("\n\n\n authy_user.content ", authy_user.content)

        if authy_user.ok():
            logger.info(
                'User {} registered to Authy Service'.format(
                    self.phone_number))
            self.authy_id = authy_user.id
            try:
                db.session.add(self)
                db.session.commit()
                return True
            except Exception as e:
                logger.exception(e)
                db.session.rollback()
                return False
        return False

    def disable_mfa(self):
        self.settings.use_mfa = False
        try:
            db.session.add(self)
            db.session.commit()
            return True
        except Exception as e:
            logger.exception(e)
            db.session.rollback()
            return False

    def enable_keypad(self):
        self.settings.use_keypad = True
        try:
            db.session.add(self)
            db.session.commit()
            return True
        except Exception as e:
            logger.exception(e)
            db.session.rollback()
            return False

    def disable_keypad(self):
        self.settings.use_keypad = False
        try:
            db.session.add(self)
            db.session.commit()
            return True
        except Exception as e:
            logger.exception(e)
            db.session.rollback()
            return False

    def send_authy_request(self, sms=False):
        response = send_authy_token_request(self.authy_id, sms=sms)
        return response

    def verify_authy_token(self, code):
        verified = verify_authy_token(self.authy_id, code)

        logger.info("\n\n")
        logger.info("Content {}".format(verified.content))
        logger.info("Errors {}".format(verified.errors()))
        logger.info("\n\n")
        if verified.ok():
            self.authy_status = 'approved'
            db.session.add(self)
            db.session.commit()
        return verified

    def delete_message(self):
        data = {
            'user_group_id': self.get_group_id,
            'user_deleted': True,
        }
        return data

    def mfa_dict(self):
        data = {
            'user_id': self.user_id,
            'group_id': self.get_group_id,
            'role_id': self.get_role_id,
            'email': self.email,
            'authy_id': self.authy_id,
            'mfa_user': self.settings.use_mfa,
        }
        return data

    @property
    def prosumer_dict(self):
        data = {
            'email': self.email,
            'mobile': self.phone_number,
            'group_id': self.get_group_id,
        }
        return data

    def change_user_role(self, role_id):
        self.role_id = self.set_role(role_id)
        db.session.add(self)
        db.session.commit()
        db.session.refresh(self)
        return self.to_dict()

    def change_password(self, new_password):
        self.password_hash = self.set_password(new_password)
        db.session.add(self)
        db.session.commit()
        db.session.refresh(self)
        return self.to_dict()

    def is_same(self, user):
        return self == user
