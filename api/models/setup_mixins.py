import bcrypt
import phonenumbers
from flask import current_app


class UserSetupMixin:
    def add_roles(self, role_list):
        if self.role in role_list:
            return role_list
        role_list.append(self.role)
        return role_list

    @staticmethod
    def set_name(name):
        return str(name.title())

    @staticmethod
    def set_phone_number(phone_number):
        input_number = phonenumbers.parse(phone_number, 'NG')
        _phone_number = phonenumbers.format_number(
            input_number, phonenumbers.PhoneNumberFormat.E164)
        return _phone_number

    @staticmethod
    def authy_mobile(phone_number):
        x = list(phone_number)
        del x[0]
        to_authy = ''.join(x)
        return to_authy

    @staticmethod
    def set_device_id(device_id):
        # TODO: Convert any meter string to match the format of a probeName
        # TODO: create validation that checks that the probe name exists in the database
        # TODO: verify that meter_id isnt a fake one and that the owner exists
        return str(device_id)

    @staticmethod
    def set_customer_device_id(customer_device_id):
        # TODO: Convert any meter string to match the format of a probeName
        # TODO: create validation that checks that the probe name exists in the database
        # TODO: verify that meter_id isnt a fake one and that the owner exists
        return str(customer_device_id)

    @staticmethod
    def set_email(email):
        return str(email.lower())

    @staticmethod
    def set_password(password):
        _strength = int(current_app.config['PASSWORD_STRENGTH'])
        return bcrypt.hashpw(
            password.encode('utf-8'),
            bcrypt.gensalt(_strength))

    def check_password(self, password):
        return bcrypt.checkpw(password.encode('utf-8'), self.password_hash)

    @staticmethod
    def set_role(role_id):
        return int(role_id)
