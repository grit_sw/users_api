from uuid import uuid4

from sqlalchemy import func

from api.models import Base
from api import db, emails
from logger import logger

from .users import Users


class Facility(Base):
    id = db.Column(db.Integer, autoincrement=True, unique=True, primary_key=True)
    name = db.Column(db.String(128), nullable=False, unique=True)
    country = db.Column(db.String(50), nullable=False)
    state = db.Column(db.String(50), nullable=False)
    city = db.Column(db.String(50), nullable=False)
    street = db.Column(db.String(50), nullable=False)
    house_no = db.Column(db.String(50), nullable=False)
    facility_id = db.Column(db.String, nullable=False, unique=True)
    facility_type = db.Column(db.String(128), nullable=False, unique=False)
    billing_method = db.Column(db.String(50))
    billing_unit = db.Column(db.String(50))

    def __repr__(self):
        return '<{} facility>'.format(
            self.name.title())

    def __init__(
        self,
        name,
        country,
        state,
        city,
        street,
        house_no,
        facility_type,
        billing_method,
        billing_unit
    ):
        """Init end consumer"""
        self.name = name.title()
        self.country = country
        self.state = state
        self.city = city
        self.street = street
        self.house_no = house_no
        self.facility_id = str(uuid4())
        self.facility_type = facility_type
        self.billing_method = billing_method
        self.billing_unit = billing_unit

    @staticmethod
    def create_facility(facility):
        """Method that saves a new shop to the database"""
        new_facility = Facility(
            name=facility['name'].title(),
            country=facility['country'].title(),
            city=facility['city'].title(),
            state=facility['state'].title(),
            street=facility['street'].title(),
            house_no=facility['house_no'].title(),
            facility_type=facility['facility_type'].title(),
            billing_method=facility['billing_method'].title(),
            billing_unit=facility['billing_unit'].title()
        )
        db.session.add(new_facility)
        try:
            db.session.commit()
        except Exception as e:
            logger.exception(e)
            db.session.rollback()
            return {}
        return new_facility.to_dict()

    @staticmethod
    def exists(facility_name=None, facility_id=None):
        if facility_name is not None:
            name = facility_name.title()
            _facility = Facility.get_facility(name=name)
            if _facility is None:
                return False
            return True
        _facility = Facility.get_facility(facility_id=facility_id)
        if _facility is None:
            return False
        return True

    @staticmethod
    def get_facility(name=None, facility_id=None):
        if name is not None:
            name = name.title()
            _facility = Facility.query.filter_by(
                name=name).first()
            return _facility
        _facility = Facility.query.filter_by(
            facility_id=facility_id).first()
        return _facility

    @staticmethod
    def all():
        facilities = Facility.query.all()
        if not facilities:
            _facilities = []
        else:
            _facilities = [facility.details() for facility in facilities]
        return _facilities

    @staticmethod
    def my_facilities(user_id):
        """
                Method to get all the facilities a user has.
        """
        _facilities = Facility.query.join(
            Facility.users, aliased=True).filter_by(
            user_id=user_id).all()
        facilities = []
        if _facilities:
            facilities = [facility.to_dict() for facility in _facilities]
        return facilities

    @staticmethod
    def invite(
        invite_clients=None,
        sender_user_id=None,
        facility_id=None,
            user_id=None):
        if user_id:
            facilities = Facility.query.join(
                Facility.users, aliased=True).filter_by(
                user_id=user_id).all()
            if not facilities:
                _facilities = []
            else:
                _facilities = [facility.to_dict() for facility in facilities]
            return _facilities

        failed_invites = []
        successful_emails_count = 0
        other_info = []
        for client in invite_clients:
            user_role = client['role']
            email = client['email']
            user = Users.get_user(email=email)
            if user:
                if not bool(user.signed_up):
                    sender = Users.get_user(user_id=sender_user_id)
                    receiver = user
                    emails.send_invite_email(receiver, sender=sender)
                    successful_emails_count += 1
                    other_info.append(
                        f"User with email:{email} has been added before and user role would not be changed, but would be sent invite again if he/she has not signed up yet.")
                    continue
                continue  # don't recreate a user if the user already exists.
            logger.info(f'Creating <NewUser {email}>')
            try:
                user_role = int(user_role)
            except (ValueError, TypeError) as e:
                logger.exception(e)
                logger.info(
                    f'Role {user_role} could npot be converted to an integer')
                continue
            new_user = Users(
                email=email.lower(),
                role_id=user_role,
                facility_id=facility_id,
            )
            db.session.add(new_user)
            try:
                db.session.commit()
            except Exception as e:
                logger.exception(e)
                logger.info('Payload failed to save properly')
                db.session.rollback()
                failed_invites.append(email)
                continue
            else:
                # Make request to auth api for token here
                sender = Users.get_user(user_id=sender_user_id)
                receiver = Users.get_user(email=email)
                emails.send_invite_email(receiver, sender=sender)
                successful_emails_count += 1
                logger.info('Added < User with email: {} >'.format(
                    email))
        return {
            'failed_emails': failed_invites,
            'successful_emails_count': successful_emails_count,
            "other_help": other_info}

    @staticmethod
    def get_count(q):
        """
                This method is used to get the count of items in a query. Its waaaay faster than len(query(...).all());
                Its about 2-3 times faster than query(...).count() because it avoids making a subquery.
                This code was found here: https://gist.github.com/hest/8798884.
        """
        count_q = q.statement.with_only_columns([func.count()]).order_by(None)
        count = q.session.execute(count_q).scalar()
        return count

    def details(self):
        json_facility = {
            'facility_name': self.name,
            'address': {
                    'country': self.country,
                    'state': self.state,
                    'city': self.city,
                    'street': self.street,
                    'house_no': self.house_no
                },
            'facility_id': self.facility_id,
            'facility_type': self.facility_type,
            'num_users': self.get_count(self.users),
            'date_created': str(self.date_created),
        }
        return json_facility

    def to_dict(self):
        json_facility = {
            'facility_name': self.name,
            'facility_id': self.facility_id,
        }
        return json_facility
