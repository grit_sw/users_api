"""Contains some common functions used in the repo"""
import re


def is_email(email):
    if re.match(r"[^@]+@[^@]+\.[^@]+", email):
        return True
    return False


def is_mobile(phone_number):
    phone_number = phone_number.replace(" ", "")
    reg_string = r"^(?=(?:.{11}|.{14})$)[+]?[0-9]*$"
    if re.match(reg_string, phone_number):
        return True
    return False
