from authy.api import AuthyApiClient

from flask import current_app


def get_authy_client():
    """Returns an AuthyApiClient"""
    return AuthyApiClient(current_app.config['AUTHY_API_KEY'])


def send_authy_token_request(authy_user_id, sms):
    """
            Sends a request to Authy to send a SMS verification code to a user's phone
    """
    client = get_authy_client()
    return client.users.request_sms(authy_user_id, {'force': sms})


def verify_authy_token(authy_user_id, user_entered_code):
    """Verifies a user-entered token with Authy"""
    client = get_authy_client()
    return client.tokens.verify(authy_user_id, user_entered_code)


def create_authy_user(email, authy_mobile, country_code):
    client = get_authy_client()
    return client.users.create(email, authy_mobile, country_code)


def delete_authy_user(authy_id):
    client = get_authy_client()
    return client.users.delete(authy_id)
