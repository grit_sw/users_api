from flask_restplus import Namespace, Resource
from flask import request

from api.manager import MFAManager, KeypadManager


user_settings_api = Namespace('user_settings',
                              description='Api for managing user settings')


@user_settings_api.route('/self-enable-mfa/')
class SelfEnableMFA(Resource):
    """
            Api for setting a user to use MFA.
    """

    @staticmethod
    def get():
        """
                HTTP method for a user to disable MFA by herself.
                @param: payload: data for the update
                @returns: response and status code
        """
        user_id = request.cookies['user_id']

        manager = MFAManager()
        resp, code = manager.enable_mfa(user_id)
        return resp, code


@user_settings_api.route('/admin-enable-mfa/<string:user_id>/')
class AdminEnableMFA(Resource):
    """
            Api for setting a user to use MFA.
    """

    @staticmethod
    def get(user_id):
        """
                HTTP method for an admin user to enable MFA for a user
                @param: payload: data for the update
                @returns: response and status code
        """
        admin_id = request.cookies['user_id']

        manager = MFAManager()
        resp, code = manager.enable_mfa(user_id, admin_id=admin_id)
        return resp, code


@user_settings_api.route('/self-disable-mfa/')
class SelfDisableMFA(Resource):
    """
            Api for a user to disable MFA
    """

    @staticmethod
    def get():
        """
                HTTP method for a user to disable MFA by herself.
                @param: payload: data for the update
                @returns: response and status code
        """
        user_id = request.cookies['user_id']

        manager = MFAManager()
        resp, code = manager.disable_mfa(user_id)
        return resp, code


@user_settings_api.route('/admin-disable-mfa/<string:user_id>/')
class AdminDisableMFA(Resource):
    """
            Api for an admin user to disable MFA for a user.
    """

    @staticmethod
    def get(user_id):
        """
                HTTP method for an admin user to disable MFA for a user.
                @returns: response and status code
        """
        admin_id = request.cookies['user_id']

        manager = MFAManager()
        resp, code = manager.disable_mfa(user_id, admin_id=admin_id)
        return resp, code


@user_settings_api.route('/self-enable-keypad/')
class SelfEnableKeypad(Resource):
    """
            Api for setting a user to use a keypad.
    """

    @staticmethod
    def get():
        """
                HTTP method for a user to enable keypad usage by herself.
                @param: payload: data for the update
                @returns: response and status code
        """
        manager = KeypadManager()
        user_id = request.cookies.get('user_id')
        resp, code = manager.enable_keypad(user_id)
        return resp, code


@user_settings_api.route('/admin-enable-keypad/<string:user_id>/')
class AdminEnableKeypad(Resource):
    """
            Api for setting a user to use keypad.
    """

    @staticmethod
    def get(user_id):
        """
                HTTP method for an admin user to enable keypad usage for a user
                @param: payload: data for the update
                @returns: response and status code
        """
        admin_id = request.cookies['user_id']

        manager = KeypadManager()
        resp, code = manager.enable_keypad(user_id, admin_id=admin_id)
        return resp, code


@user_settings_api.route('/self-disable-keypad')
class SelfDisableKeypad(Resource):
    """
            Api for a user to disable Keypad
    """

    @staticmethod
    def get():
        """
                HTTP method for a user to disable keypad usage by herself.
                @param: payload: data for the update
                @returns: response and status code
        """
        user_id = request.cookies['user_id']

        manager = KeypadManager()
        resp, code = manager.disable_keypad(user_id)
        return resp, code


@user_settings_api.route('/admin-disable-keypad/<string:user_id>/')
class AdminDisableKeypad(Resource):
    """
            Api for an admin user to disable keypad usage for a user.
    """

    @staticmethod
    def get(user_id):
        """
                HTTP method for an admin user to disable keypad usage for a user.
                @returns: response and status code
        """
        admin_id = request.cookies['user_id']

        manager = KeypadManager()
        resp, code = manager.disable_keypad(user_id, admin_id=admin_id)
        return resp, code
