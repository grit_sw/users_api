from logger import logger


def throw_400_error(e):
    """
    Throw a HTTP 400 error
    :param e: Exception caught
    :return: response dict and Http status code
    """
    logger.exception(e.messages)
    response = dict()
    response['success'] = False
    response['errors'] = e.messages
    return response, 400
