import json

from flask import current_app, request
from flask_restplus import Namespace, Resource, fields
from marshmallow import ValidationError

from api import api
from api.controllers.common import throw_400_error
from api.manager import UserManager
from api.schema import EditMeSchema, EditUserSchema, ConfirmPasswordSchema
from logger import logger


users_api = Namespace('users', description='Api for managing app users')

edit_me = users_api.model('Edit Me', {
    'user_id': fields.String(required=False, description='The user\'s ID'),
    'first_name': fields.String(required=False, description='The user\'s first name'),
    'last_name': fields.String(required=False, description='The user\'s last name'),
    'email': fields.String(required=False, description='The user\'s email address'),
    'phone_number': fields.String(required=True, description='The user\'s phone number'),
    'use_mfa': fields.Boolean(required=True, description='The user\'s mfa preference'),
})

edit_user = users_api.model('Edit a user', {
    'user_id': fields.String(required=False, description='The user\'s ID'),
    'first_name': fields.String(required=False, description='The user\'s first name'),
    'last_name': fields.String(required=False, description='The user\'s last name'),
    'email': fields.String(required=False, description='The user\'s email address'),
    'phone_number': fields.String(required=True, description='The user\'s phone number'),
    'use_mfa': fields.Boolean(required=True, description='The user\'s mfa preference'),
    'gtg_user': fields.Boolean(required=True, description='Is this user a GTG user'),
    'configuration_user': fields.Boolean(required=True, description='Is the user a configuration user?'),
    'use_keypad': fields.Boolean(required=True, description='Keypad user indicator'),
})

update_role = users_api.model('Update a user\'s role', {
    'user_id': fields.String(required=False, description='The user\'s ID'),
    'role_id': fields.String(required=False, description='The new role ID'),
})

change_password = users_api.model(
    'Update a user\'s password', {
        'new_password': fields.String(
            required=False, description='The new password'), 'confirm_password': fields.String(
                required=False, description="Confirmation of password set"), })

fake_invite = users_api.model('Create a fake Invite', {
    'email': fields.String(required=False, description='The user\'s email'),
    'role_id': fields.Integer(required=False, description='The user\'s role_id'),
    'facility_id': fields.String(required=False, description='The user\'s facility_id'),
    'parent_id': fields.String(required=False, description='The user\'s parent_id'),
    'use_mfa': fields.Boolean(required=False, description='Is this an MFA user?'),
    'use_keypad': fields.Boolean(required=False, description='Is this a keypad user?'),
    'is_active': fields.Boolean(required=False, description='Should the user be immediately activated?'),

})

fake_signup = users_api.model('Create a fake signup', {
    'first_name': fields.String(required=False, description='The user\'s first_name'),
    'last_name': fields.String(required=False, description='The user\'s last_name'),
    'password': fields.String(required=False, description='The user\'s password'),
    'phone_number': fields.String(required=False, description='The user\'s phone_number'),
})

new_fake_user = users_api.model('Create a fake user', {
    'invite': fields.Nested(fake_invite, required=False, description='The user\'s invite data.'),
    'signup': fields.Nested(fake_signup, required=False, description='The user\'s signup data.'),
})

new_fake_users = users_api.model(
    'Create many fake users', {
        'users': fields.List(
            fields.Nested(
                new_fake_user, required=False, description='The user\'s data.')), })


@users_api.route('/search/')
class Search(Resource):
    """
            API to view a user's details by a user with a senior role
    """

    @staticmethod
    def get():
        """
                HTTP method for a user to view his/her details
                @param: payload: data for the update
                @returns: response and status code
        """
        user_id = request.cookies.get('user_id')
        if not user_id:
            response_message = {
                "success": True,
                "message": 'Unauthourized.'
            }
            response = current_app.response_class(
                response=json.dumps(response_message),
                status=403,
                mimetype='application/json'
            )
            return response

        search_term = request.args.get('query', type=str)
        search_role = request.args.get('role_id', type=str)
        facility_id = request.args.get('facility_id', type=str)
        size = request.args.get('size', 20, type=int)
        manager = UserManager()
        resp, code = manager.search(
            search_term, size, user_id, facility_id=facility_id, search_role=search_role)
        return resp, code


@users_api.route('/me/')
class Me(Resource):
    """
            API to view a user's details by a user with a senior role
    """

    @staticmethod
    def get():
        """
                HTTP method for a user to view his/her details
                @param: payload: data for the update
                @returns: response and status code
        """
        user_id = request.cookies.get('user_id')
        if not user_id:
            response_message = {
                "success": True,
                "message": 'Unauthourized.'
            }
            response = current_app.response_class(
                response=json.dumps(response_message),
                status=403,
                mimetype='application/json'
            )
            return response
        manager = UserManager()
        resp, code = manager.me(user_id)
        return resp, code


@users_api.route('/edit-me/')
class EditMe(Resource):
    """
            API for a user to edit her details.
    """

    @staticmethod
    @users_api.expect(edit_me)
    def put():
        """
                HTTP method for a user to edit his/her details
                @param: payload: data for the update
                @returns: response and status code
        """
        user_id = request.cookies.get('user_id')
        if not user_id:
            response_message = {
                "success": True,
                "message": 'Unauthourized.'
            }
            response = current_app.response_class(
                response=json.dumps(response_message),
                status=403,
                mimetype='application/json'
            )
            return response

        schema = EditMeSchema(strict=True)
        data = request.values.to_dict()
        payload = api.payload or data
        try:
            new_payload = schema.load(payload).data._asdict()
        except ValidationError as e:
            logger.exception(e.messages)
            response = {}
            response['success'] = False
            response['errors'] = e.messages
            return response, 400

        manager = UserManager()
        resp, code = manager.edit_me(user_id, new_payload)
        return resp, code


@users_api.route('/edit-user/')
class EditUser(Resource):
    """
            API for a user to edit her details.
    """

    @staticmethod
    @users_api.expect(edit_user)
    def put():
        """
                HTTP method for a user to edit his/her details
                @param: payload: data for the update
                @returns: response and status code
        """
        viewer_id = request.cookies.get('user_id')
        role_id = request.cookies.get('role_id')
        if not viewer_id and not role_id:
            response_message = {
                "success": True,
                "message": 'Unauthourized.'
            }
            response = current_app.response_class(
                response=json.dumps(response_message),
                status=403,
                mimetype='application/json'
            )
            return response

        if viewer_id or int(role_id) == 6:
            schema = EditUserSchema(strict=True)
            data = request.values.to_dict()
            logger.info(f"Data to enter {data}")
            payload = api.payload or data
            logger.info(f"Payload entering {payload}")
            try:
                new_payload = schema.load(payload).data._asdict()
            except ValidationError as e:
                return throw_400_error(e)

            manager = UserManager()
            resp, code = manager.edit_user(new_payload)
            return resp, code
        response_message = {
            "success": True,
            "message": 'Unauthourized.'
        }
        response = current_app.response_class(
            response=json.dumps(response_message),
            status=403,
            mimetype='application/json'
        )
        return response


@users_api.route('/id/<string:user_id>/')
class GetUser(Resource):
    """
            API to manage individual users
    """

    @staticmethod
    def get(user_id):
        """
                HTTP method to get a user's details by a user with a senior role
                @param: customer_id: ID of the customer
                @returns: response and status code
        """
        viewer_id = request.cookies.get('user_id')
        role_id = request.cookies.get('role_id')

        if not viewer_id and not role_id:
            response_message = {
                "success": True,
                "message": 'Unauthourized.'
            }
            response = current_app.response_class(
                response=json.dumps(response_message),
                status=403,
                mimetype='application/json'
            )
            return response
        manager = UserManager()
        resp, code = manager.retrieve_user(user_id, viewer_id=viewer_id, role_id=role_id)
        return resp, code


@users_api.route('/in-facility/<string:facility_id>/')
class AllUsersInAFacility(Resource):
    """
            PUBLIC API to view all the users in a facility
    """
    #
    # @api.doc(security='apikey')
    # @users_api.marshal_with(user, code=201)
    # @users_api.marshal_with(user)

    @staticmethod
    def get(facility_id):
        """
                PUBLIC API to view all the users in a facility
                @param: payload: data for the update
                @returns: response and status code
        """
        user_id = request.cookies.get('user_id')
        if not user_id:
            response_message = {
                "success": True,
                "message": 'Unauthourized.'
            }
            response = current_app.response_class(
                response=json.dumps(response_message),
                status=403,
                mimetype='application/json'
            )
            return response
        manager = UserManager()
        page = request.args.get('page', 1, type=int)
        resp, code = manager.all_users_in_a_facility(
            page, user_id, facility_id)
        return resp, code


@users_api.route('/all/')
class AllUsers(Resource):
    """
            PRIVATE API to view all the users in all facilities
    """
    #
    # @api.doc(security='apikey')
    # @users_api.marshal_with(user, code=201)
    # @users_api.marshal_with(user)

    @staticmethod
    def get():
        """
                PRIVATE API to view all the users in all facilities
                @param: payload: data for the update
                @returns: response and status code
        """
        user_id = request.cookies['user_id']
        manager = UserManager()
        page = request.args.get('page', 1, type=int)
        resp, code = manager.all_users(page, user_id)
        return resp, code


@users_api.route('/configuration/all/')
class AllConfigurationUsers(Resource):
    """
            PRIVATE API to view all the users in all facilities
    """
    #
    # @api.doc(security='apikey')
    # @users_api.marshal_with(user, code=201)
    # @users_api.marshal_with(user)

    @staticmethod
    def get():
        """
                PRIVATE API to view all the configuration users in all facilities
                @param: payload: data for the update
                @returns: response and status code
        """
        user_id = request.cookies['user_id']
        manager = UserManager()
        page = request.args.get('page', 1, type=int)
        resp, code = manager.all_config_users(page, user_id)
        return resp, code


@users_api.route('/role/all/')
class ViewRole(Resource):
    """
            API for viewing all roles
    """

    @staticmethod
    def get():
        """
                HTTP method for viewing all roles
                @param: payload: data for the update
                @returns: response and status code
        """
        user_id = request.cookies['user_id']
        manager = UserManager()
        resp, code = manager.all_roles(user_id)
        return resp, code


@users_api.route('/by-role/<string:role_id>/')
class AllUsersOfRoleInAllFacilities(Resource):
    """
            API to view all the users in all facilities with a particular role
    """

    @staticmethod
    def get(role_id):
        """
                PRIVATE API to view all the users in all facilities with a particular role
                @param: payload: data for the update
                @returns: response and status code
        """
        user_id = request.cookies['user_id']
        manager = UserManager()
        page = request.args.get('page', 1, type=int)
        resp, code = manager.all_users_in_all_facilities_with_role(
            page, user_id, role_id)
        return resp, code


@users_api.route('/by-role-facility/<string:role_id>/<string:facility_id>/')
class AllUsersWithRoleInOneFacility(Resource):
    """
            API to view all the users in one facility with a particular role.
    """

    @staticmethod
    def get(role_id, facility_id):
        """
                View all the users in one facility with a role, useful for prosumer assigment.
                @param: payload: data for the update
                @returns: response and status code
        """
        user_id = request.cookies['user_id']
        manager = UserManager()
        page = request.args.get('page', 1, type=int)
        resp, code = manager.all_users_in_one_facility_with_role(
            page, user_id, role_id, facility_id)
        return resp, code


@users_api.route('/max-admin/<string:group_id>/')
class MaxAdminInAGroup(Resource):
    """
            API to view the highest ranking admin in a group
    """

    @staticmethod
    def get(group_id):
        """
                PUBLIC API to view the highest ranking admin in a group
                @param: payload: data for the update
                @returns: response and status code
        """
        manager = UserManager()
        resp, code = manager.max_admin_in_group(group_id)
        return resp, code


@users_api.route('/by-group/<string:group_id>/')
class AllUsersInAGroup(Resource):
    """
            API to view all the users in a group
    """

    @staticmethod
    def get(group_id):
        """
                PUBLIC API to view all the users in a group
                @param: payload: data for the update
                @returns: response and status code
        """
        user_id = request.cookies['user_id']
        manager = UserManager()
        page = request.args.get('page', 1, type=int)
        resp, code = manager.all_users_in_a_group(page, user_id, group_id)
        return resp, code


@users_api.route('/delete/<string:user_id>/')
class DeleteUser(Resource):
    """
            API to manage individual users
    """

    @staticmethod
    def delete(user_id):
        """
                HTTP method to delete a user's details by a user with a senior role
                @param: customer_id: ID of the customer
                @returns: response and status code
        """
        viewer_id = request.cookies['user_id']
        manager = UserManager()
        resp, code = manager.delete_user(user_id, viewer_id)
        return resp, code


@users_api.route('/change-role/')
class ChangeUserRole(Resource):
    """
            API to modify a user's role
    """

    @staticmethod
    @users_api.expect(update_role)
    def put():
        """
                HTTP method to update a user's role by a user with a senior role
                @param: customer_id: ID of the customer
                @returns: response and status code
        """
        performer_id = request.cookies.get('user_id')
        if not performer_id:
            response_message = {
                "success": False,
                "message": 'Unauthourized.'
            }
            response = current_app.response_class(
                response=json.dumps(response_message),
                status=403,
                mimetype='application/json'
            )
            return response

        data = request.values.to_dict()
        payload = api.payload or data

        manager = UserManager()
        resp, code = manager.change_user_role(
            performer_id, payload['user_id'], payload['role_id'])
        return resp, code


@users_api.route('/change-password/')
class ChangeMyPassword(Resource):
    """
            API to modify a user's password
    """

    @staticmethod
    @users_api.expect(change_password)
    def put():
        """
                HTTP method to change my password
                @returns: response and status code
        """
        user_id = request.cookies.get('user_id')
        if not user_id:
            response_message = {
                "success": False,
                "message": 'Unauthourized.'
            }
            response = current_app.response_class(
                response=json.dumps(response_message),
                status=403,
                mimetype='application/json'
            )
            return response

        data = request.values.to_dict()
        payload = api.payload or data
        schema = ConfirmPasswordSchema(strict=True)
        try:
            new_payload = schema.load(payload).data._asdict()
        except ValidationError as e:
            return throw_400_error(e)

        manager = UserManager()
        resp, code = manager.change_password(user_id, new_payload)
        return resp, code


@users_api.route('/new-fake-user/')
class CreateFakeUser(Resource):
    """
            API to create a fake user
    """

    @staticmethod
    @users_api.expect(new_fake_users)
    def post():
        """
                HTTP method to create a fake user
                @returns: response and status code
        """
        if current_app.config.get('FLASK_ENV') in {
                'prod', 'production', 'Production'}:
            response_message = {
                "success": False,
                "message": 'Invalid Url.'
            }
            response = current_app.response_class(
                response=json.dumps(response_message),
                status=404,
                mimetype='application/json'
            )
            return response

        data = request.values.to_dict()
        payload = api.payload or data

        manager = UserManager()
        resp, code = manager.create_fake_user(payload)
        return resp, code
