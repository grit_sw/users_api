import json

from flask import current_app, request
from flask_restplus import Namespace, Resource, fields
from marshmallow import ValidationError

from api import api
from api.controllers.common import throw_400_error
from api.manager import UserGroupManager
from api.schema import EditUserGroupSchema


users_group_api = Namespace(
    'user_groups',
    description='Api for managing app user groups')

edit_user_group = users_group_api.model('Edit a user group', {
    'group_id': fields.String(required=True, description='The id of the usergroup'),
    'blockchain_address': fields.String(required=True, description='The blockchain address'),
    'blockchain_private_key': fields.String(required=True, description='The blockchain privatekey'),
})


@users_group_api.route('/id/<string:group_id>/')
class GetUserGroup(Resource):
    """
            API to manage user group
    """

    @staticmethod
    def get(group_id):
        """
                HTTP method to get a user's details by a user with a senior role
                @param: customer_id: ID of the customer
                @returns: response and status code
        """
        role_id = request.cookies.get('role_id')

        if not role_id:
            response_message = {
                "success": True,
                "message": 'Unauthourized.'
            }
            response = current_app.response_class(
                response=json.dumps(response_message),
                status=403,
                mimetype='application/json'
            )
            return response

        manager = UserGroupManager()
        resp, code = manager.retrieve_user_group(
            group_id=group_id, role_id=role_id)
        return resp, code


@users_group_api.route('/blockchain-address/<string:blockchain_address>/')
class GetUserGroupbyBlockchain(Resource):
    """
            API to manage user group
    """

    @staticmethod
    def get(blockchain_address):
        """
                HTTP method to get a user group using
                @:param blockhain_address : The blockchain address of user group.
                @returns: response and status code
        """
        role_id = request.cookies.get('role_id')

        if not role_id:
            response_message = {
                "success": True,
                "message": 'Unauthourized.'
            }
            response = current_app.response_class(
                response=json.dumps(response_message),
                status=403,
                mimetype='application/json'
            )
            return response

        manager = UserGroupManager()
        resp, code = manager.retrieve_user_group(
            blockchain_address=blockchain_address, role_id=role_id)
        return resp, code


@users_group_api.route('/edit-group/')
class EditUserGroup(Resource):

    @staticmethod
    @users_group_api.expect(edit_user_group)
    def put():
        """
                HTTP method for a user to edit his/her details
                @param: payload: data for the update
                @returns: response and status code
        """
        role_id = request.cookies.get('role_id')
        if not role_id:
            response_message = {
                "success": True,
                "message": 'Unauthourized.'
            }
            response = current_app.response_class(
                response=json.dumps(response_message),
                status=403,
                mimetype='application/json'
            )
            return response

        schema = EditUserGroupSchema(strict=True)
        data = request.values.to_dict()
        payload = api.payload or data
        try:
            new_payload = schema.load(payload).data._asdict()
        except ValidationError as e:
            return throw_400_error(e)

        manager = UserGroupManager()
        resp, code = manager.edit_group(payload=new_payload, role_id=role_id)
        return resp, code


@users_group_api.route('/all/')
class AllUserGroups(Resource):

    @staticmethod
    def get():
        """
                HTTP method to view all user groups
                @returns: response and status code
        """
        role_id = request.cookies.get('role_id')
        if not role_id:
            response_message = {
                "success": True,
                "message": 'Unauthourized.'
            }
            response = current_app.response_class(
                response=json.dumps(response_message),
                status=403,
                mimetype='application/json'
            )
            return response

        manager = UserGroupManager()
        resp, code = manager.all_groups(role_id=role_id)
        return resp, code
