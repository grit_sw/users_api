from marshmallow import ValidationError

from flask import request
from flask_restplus import Namespace, Resource, fields

from api import api
from api.manager import InvitationManager
from api.schema import InvitationSchema
from api.exceptions import AuthourisationError, NotFoundError
from logger import logger

invite_api = Namespace('invite', description='Api for managing app signup')

user_email = invite_api.model(
    'UserEmail', {
        'email': fields.String(
            required=True, description='Email of a user to invite.')})

new_invite = invite_api.model('NewInvite', {
    'emails': fields.List(fields.Nested(user_email), required=True, description='A list of emails for an invite'),
    'role_id': fields.String(required=False, description='The new user\'s role ID'),
    'mfa_required': fields.Boolean(required=True, description='MFA code required'),
    'gtg_user': fields.Boolean(required=True, description='Is this user a GTG user'),
    'disco_user': fields.Boolean(required=True, description='Is this user a DISCO user'),
    'configuration_user': fields.Boolean(required=True, description='Is the user a configuration user?'),
    'keypad_user': fields.Boolean(required=True, description='Keypad user indicator required'),
    'facility_id': fields.String(required=True, description='The User\'s facility'),

})


@invite_api.route('/new/')
class Invite(Resource):
    """
            Api to create a new energy user
    """

    @staticmethod
    def get():
        """
                HTTP method for viewing all roles
                @param: payload: data for the update
                @returns: response and status code
        """
        user_id = request.cookies.get('user_id')
        manager = InvitationManager()
        resp, code = manager.invite_params(user_id)
        return resp, code

    @staticmethod
    @invite_api.expect(new_invite)
    def post():
        """
                HTTP method for user login
                @param: payload: data for the update
                @returns: response and status code
        """
        # Protect this route via tokens.
        schema = InvitationSchema(strict=True, many=True)
        data = request.values.to_dict()
        payload = api.payload or data
        payload['user_id'] = request.cookies.get('user_id')
        payload['emails'] = list(set(payload['emails']))
        response = {}
        try:
            new_payload = schema.load([payload]).data[0]._asdict()
        except ValidationError as e:
            logger.exception(e.messages)
            response['success'] = False
            response['errors'] = e.messages
            return response, 400

        except AuthourisationError as e:
            logger.exception(e)
            response['success'] = False
            response['errors'] = e.args[0]
            return response, 403

        except NotFoundError as e:
            logger.exception(e)
            response['success'] = False
            response['errors'] = e.args[0]
            return response, 404

        manager = InvitationManager()
        resp, code = manager.new_invite(new_payload)
        return resp, code


@invite_api.route('/accept/<string:email>/')
class Accept(Resource):
    """
            Api to create a new energy user
    """

    @staticmethod
    def get(email):
        manager = InvitationManager()
        resp, code = manager.set_invite_clicked(email)
        return resp, code


@invite_api.route('/status/<string:email>/')
class InviteStatus(Resource):
    """
            Api to create a new energy user
    """

    @staticmethod
    def get(email):
        manager = InvitationManager()
        user_id = request.cookies['user_id']
        resp, code = manager.check_status(user_id, email)
        return resp, code


@invite_api.route('/resend/')
class Resend(Resource):
    """
            Api to resend an invite to a user
    """

    @staticmethod
    def get():
        manager = InvitationManager()
        email = request.args.get("email", type=str)
        if not email:
            response = {}
            response['success'] = False
            response['message'] = 'Email required'
            return response, 400
        resp, code = manager.resend_invite(email)
        return resp, code
