from marshmallow import ValidationError
from flask import request
from flask_restplus import Namespace, Resource, fields

from api import api
from api.manager import FacilityManager
from api.schema import NewFacilitySchema
from logger import logger

facility_api = Namespace(
    'facilities',
    description='Api for managing facilities and facilities')

# todo need to specify that invite clients is a list of jsons
facility = facility_api.model('Facility', {
    'name': fields.String(required=True, description='The name of the facility'),
    "country": fields.String(required=True),
    "state": fields.String(required=True),
    "city": fields.String(required=True, ),
    "street": fields.String(required=True),
    "house_no": fields.String(required=True),
    "facility_type": fields.String(required=True),
    'billing_method': fields.String(description="Billing method for facility"),
    'billing_unit': fields.String(required=True),
    'invite_clients': fields.List(fields.List(fields.String), required=True, description="List of Roles and emails of users to be added"),
})

invite_clients = facility_api.model("ClientsInvites", {
    'invite_clients': fields.List(fields.List(fields.String), required=True),
})


@facility_api.route('/new/')
class CreateFacility(Resource):
    """
            Api to create one or more facilities
    """

    @staticmethod
    @facility_api.expect(facility)
    def post():
        """
                HTTP method to create one or more facilities
                @param: customer_id: ID of the customer
                @returns: response and status code
        """
        response = {}
        data = request.values.to_dict()
        payload = api.payload or data
        schema = NewFacilitySchema(strict=True)
        try:
            new_payload = schema.load(payload).data._asdict()
        except ValidationError as e:
            logger.exception(e.messages)
            response['success'] = False
            response['errors'] = e.messages
            return response, 400

        role_id = request.cookies.get('role_id')
        user_id = request.cookies.get('user_id')
        try:
            role_id = int(role_id)
        except ValueError as e:
            logger.exception(e)
            response['success'] = False
            response['message'] = 'Unauthourised.'
            return response, 403

        if role_id < 5:
            response['success'] = False
            response['message'] = 'Unauthourised.'
            return response, 403

        manager = FacilityManager()
        resp, code = manager.create_facility(new_payload, user_id)
        return resp, code


@facility_api.route('/all/')
class AllFacilities(Resource):

    """
            Api to manage all facilities
    """

    @staticmethod
    def get():
        """
                HTTP method to view a facility
                @param: customer_id: ID of the customer
                @returns: response and status code
        """

        role_id = request.cookies.get('role_id')
        response = {}
        try:
            role_id = int(role_id)
        except ValueError as e:
            logger.exception(e)
            response['success'] = False
            response['message'] = 'Unauthourised.'
            return response, 403

        if role_id < 5:
            response['success'] = False
            response['message'] = 'Unauthourised.'
            return response, 403

        manager = FacilityManager()
        resp, code = manager.all(role_id)
        return resp, code


@facility_api.route('/me/')
class MyFacilities(Resource):
    """
            Api to manage a user's facility
    """

    @staticmethod
    def get():
        """
                HTTP method to view all the facilities a user has.
                @param: customer_id: ID of the customer
                @returns: response and status code
        """
        user_id = request.cookies['user_id']
        manager = FacilityManager()
        resp, code = manager.my_facilities(user_id)
        return resp, code


@facility_api.route('/<string:facility_id>/')
class FacilityID(Resource):
    """
            Api to manage a facility by its ID
    """

    @staticmethod
    def get(facility_id):
        """
                HTTP method to view a facility by its ID
                @param: customer_id: ID of the customer
                @returns: response and status code
        """
        response = {}
        role_id = request.cookies.get('role_id')
        try:
            int(role_id)
        except ValueError as e:
            logger.exception(e)
            response['success'] = False
            response['message'] = 'Unauthourised.'
            return response, 403

        manager = FacilityManager()
        resp, code = manager.view_facility(facility_id)
        return resp, code


@facility_api.route('/invite/')
class FacilityInvite(Resource):
    @staticmethod
    @facility_api.expect(invite_clients)
    def post():
        response = {}
        facility_id = request.cookies.get("facility_id")
        user_id = request.cookies.get("user_id")
        data = request.values.to_dict()
        payload = api.payload or data
        schema = NewFacilitySchema()
        try:
            new_payload = schema.load(
                payload, partial=(
                    'invite_clients',)).data
            logger.info(f"New payload is {new_payload}")
        except ValidationError as e:
            logger.exception(e.messages)
            response['success'] = False
            response['errors'] = e.messages
            return response, 400

        manager = FacilityManager()
        resp, code = manager.invite(
            invite_clients=new_payload['invite_clients'], sender_user_id=user_id, facility_id=facility_id)
        return resp, code
