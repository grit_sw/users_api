from marshmallow import ValidationError
from flask_restplus import Namespace, Resource, fields

from flask import request

from api import api
from api.controllers.common import throw_400_error
from api.exceptions import NotFoundError
from api.manager import LoginManager, SignUpManager
from api.schema import LoginSchema, SignUpSchema
from logger import logger


auth_api = Namespace('auth', description='Api for managing app signup')

login = auth_api.model('NewLogin', {
	'user_id': fields.String(required=True, description='User email or phone number.'),
	'password': fields.String(required=True, description='The user\'s password.'),
})

signup = auth_api.model('SignUp', {
	'first_name': fields.String(required=True, description='The user\'s first name'),
	'last_name': fields.String(required=True, description='The user\'s last name'),
	'email': fields.String(required=True, description='The user\'s email'),
	'password': fields.String(required=True, description='User\'s password'),
	'confirm_password': fields.String(required=True, description='User\'s password'),
	'phone_number': fields.String(required=False, description='The new user\'s phone number'),
})


@auth_api.route('/login/')
class Login(Resource):
	"""
			Api for user login
	"""

	@staticmethod
	@auth_api.expect(login)
	def post():
		"""
				HTTP method for user login
				@param: payload: data for the update
				@returns: response and status code
		"""
		schema = LoginSchema(strict=True)
		data = request.values.to_dict()
		payload = api.payload or data
		try:
			# new_payload = schema.load(payload).data._asdict()  # Not sure why this is failing
			new_payload = schema.load(payload).data
		except ValidationError as e:
			return throw_400_error(e)

		manager = LoginManager()

		resp, code = manager.login(**new_payload)
		return resp, code


@auth_api.route('/signup/')
class SignUp(Resource):
	"""
		Api to create a new energy user
	"""

	@auth_api.expect(signup)
	def post(self):
		"""
			HTTP method for user signup
			@param: payload: data for the update
			@returns: response and status code
		"""
		# Protect this route via tokens.
		schema = SignUpSchema(strict=True)
		data = request.values.to_dict()
		payload = api.payload or data

		try:
			new_payload = schema.load(payload).data._asdict()
		except ValidationError as e:
			logger.exception(e.messages)
			response = {}
			response['success'] = False
			response['errors'] = e.messages
			return response, 400

		except NotFoundError as e:
			logger.exception(e.messages)
			response = {}
			response['success'] = False
			response['errors'] = e.messages
			return response, 404

		manager = SignUpManager()
		resp, code = manager.signup(new_payload)
		return resp, code


@auth_api.route('/sudo/')
class Sudo(Resource):
	"""
		Api to get a sudo user. Delete in production. Only used by cmdctrl
	"""

	def get(self):
		"""
			HTTP method to get a sudo user. Delete in production. Only used by cmdctrl
			@param: payload: data for the update
			@returns: response and status code
		"""
		manager = LoginManager()
		resp, code = manager.get_sudo()
		return resp, code
