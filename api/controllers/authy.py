from marshmallow import ValidationError
from flask_restplus import Namespace, Resource, fields

from flask import request

from api import api
from api.exceptions import NotFoundError
from api.manager import AuthyManager
from api.schema import AuthySchema
from logger import logger


authy_api = Namespace('authy', description='Api for managing app signup')
authy_code = authy_api.model('AuthyCode', {
    'authy_code': fields.String(required=True, description='Authy code'),
})


@authy_api.route('/verify/')
class AuthyVerify(Resource):
    """
            Api to create a new energy user
    """

    @staticmethod
    @authy_api.expect(authy_code)
    def post():
        """
                HTTP method for authy input code
                @param: payload: data for the update
                @returns: response and status code
        """
        # Protect this route via tokens.
        schema = AuthySchema(strict=True)
        data = request.values.to_dict()
        payload = api.payload or data
        payload['user_id'] = request.cookies['user_id']
        try:
            new_payload = schema.load(payload).data._asdict()
        except ValidationError as e:
            logger.exception(e.messages)
            response = {}
            response['success'] = False
            response['errors'] = e.messages
            return response, 400

        except NotFoundError as e:
            logger.exception(e)
            response = {}
            response['success'] = False
            response['errors'] = str(e)
            return response, 404

        manager = AuthyManager()
        resp, code = manager.verify_authy_token(**new_payload)
        return resp, code


@authy_api.route('/send-sms/')
class AuthySend(Resource):
    """
            Api to create a new energy user
    """

    @staticmethod
    def get():
        """
                HTTP method for sending an authy code to a particular user.
                @param: payload: data for the update
                @returns: response and status code
        """
        cookies = request.cookies
        user_id = cookies['user_id']
        manager = AuthyManager()
        resp, code = manager.send_authy_sms(user_id)
        return resp, code
