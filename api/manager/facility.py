from api.exceptions import UUIDClashError
from api.models import Facility
from logger import logger


class FacilityManager:
    """Class for facility and prosumer operations"""

    def create_facility(self, payload, user_id):
        """
                Method to create new facility
                @args: payload payload
                @returns: a status message and status code
        """
        response = {}
        try:
            data = Facility.create_facility(payload)
        except (UUIDClashError, Exception) as e:
            logger.exception(e)
            response['success'] = False
            response['message'] = 'Service Unavailable.'
            return response, 503
        response['success'] = True
        response['data'] = data
        invite_clients = payload['invite_clients']
        if invite_clients:
            invite_details = self.invite(
                invite_clients, user_id, data['facility_id'])
            response['invite_details'] = invite_details
        return response, 201

    @staticmethod
    def all(role_id):
        response = {}
        if int(role_id) < 5:
            response['success'] = False
            response['message'] = "You can't view this resource."
            return response, 403

        data = Facility.all()
        response['success'] = True
        response['count'] = len(data)
        response['data'] = data
        print("data is your")
        print(data)
        return response, 200

    @staticmethod
    def view_facility(facility_id):
        response = {}
        if not Facility.exists(facility_id=facility_id):
            response['success'] = False
            response['message'] = 'Facility Not Found.'
            return response, 404

        data = Facility.get_facility(facility_id=facility_id).details()
        response['success'] = True
        response['count'] = len(data)
        response['data'] = data
        return response, 200

    @staticmethod
    def my_facilities(user_id):
        response = {}
        data = Facility.my_facilities(user_id)
        response['success'] = True
        response['count'] = len(data)
        response['data'] = data
        return response, 200

    @staticmethod
    def invite(invite_clients, sender_user_id, facility_id):
        response = {}
        if invite_clients:
            try:
                data = Facility.invite(
                    invite_clients=invite_clients,
                    sender_user_id=sender_user_id,
                    facility_id=facility_id)
            except Exception as e:
                logger.info("Something went wrong {}".format(e))
                response['success'] = False
                return response, 500
            response['data'] = data
            response['success'] = True
            return response, 200
        response['data'] = "No invite clients"
        response['success'] = True
        return response, 200
