from authy import AuthyFormatException

from api.models import Users
from logger import logger


class AuthyManager:
    """docstring for AuthyManager"""

    @staticmethod
    def send_authy_sms(user_id):
        response = {}
        user = Users.get_user(user_id=user_id)
        if user is None:
            response['success'] = False
            response['message'] = 'User not found'
            return response, 404

        if not user.settings.use_mfa:
            response['success'] = False
            response['message'] = 'Multi-factor authentication not enabled for this user'
            return response, 403

        if not user.authy_id:
            response['success'] = False
            response['message'] = 'User not registered to authy service.'
            return response, 403

        try:
            send_sms = user.send_authy_request(sms=True)
        except Exception as e:
            logger.exception(e)
            response['success'] = False
            response['message'] = 'Authy Error'
            return response, 400

        del send_sms.content['cellphone']
        response = send_sms.content
        return response, send_sms.response.status_code

    @staticmethod
    def verify_authy_token(user_id, authy_code):
        response = {}
        user = Users.get_user(user_id=user_id)
        logger.info("\n\n\n\n")
        logger.info(user.to_dict())
        logger.info("\n\n\n\n")
        if user is None:
            response['success'] = False
            response['message'] = 'User not found'
            return response, 404

        if not user.settings.use_mfa:
            response['success'] = False
            response['message'] = 'Multi-factor authentication not enabled for this user'
            return response, 403

        if not user.authy_id:
            response['success'] = False
            response['message'] = 'User not registered to authy service.'
            return response, 403

        try:
            verified = user.verify_authy_token(str(authy_code))
        except AuthyFormatException as e:
            logger.exception(e)
            response['success'] = False
            response['message'] = 'Authy Code Format Error'
            return response, 400
        if verified.ok():
            response['success'] = True
            response['data'] = user.to_dict()
            return response, 201

        response['success'] = False
        response['message'] = 'Code invalid - please try again.'
        return response, 400
