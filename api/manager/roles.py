from api.models import Role, Users


class RoleManager:
    """Class to manage user creation and retrieval"""

    def __init__(self, user_api):
        self.user_api = user_api

    def get_all_roles(self, email=None, mobile=None):
        """
            Method to get end users associated with a client admin
            @param: client_admin_id: ID of the customer's client admin
            @param: customer_id: ID of the customer
            @returns: a status message and status code
        """

        user = Users.get_user(email=email, phone_number=mobile)

        if user is None:
            self.user_api.abort(404, "Admin not found")

        payload = Role.to_json()

        return payload, 200

    def view_my_permissions(self, user_email=None, user_mobile=None):
        """
            Method to get end users associated with a client admin
            @param: client_admin_id: ID of the customer's client admin
            @param: customer_id: ID of the customer
            @returns: a status message and status code
        """
