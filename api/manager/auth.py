from flask import request

from api.common import is_email, is_mobile
from api.exceptions import UUIDClashError
from api.models import Users

from logger import logger


class LoginManager(object):
	"""
		Class to manage user creation and retrieval
	"""

	def login(self, user_id, password):
		response = {}
		if is_email(user_id):
			user = Users.get_user(email=user_id)
		elif is_mobile(user_id):
			user = Users.get_user(phone_number=user_id)
		else:
			response['success'] = False
			response['message'] = 'Email address or phone number required.'
			return response, 401

		if user is None:
			response['success'] = False
			if is_email(user_id):
				response['message'] = 'Email or password incorrect.'
			elif is_mobile(user_id):
				response['message'] = 'Phone number or password incorrect.'
			else:
				response['message'] = 'Email or phone number required.'
			return response, 401

		if not bool(user.signed_up):
			response['success'] = False
			response['message'] = "You are not signed up yet. Please check your email for your invite."
			return response, 403

		cookies = request.cookies
		uuid = cookies.get('user_id')
		if user.user_id == uuid:
			response['success'] = False
			response['message'] = 'User Already logged In.'
			return response, 201
		try:
			password_match = user.check_password(password)
		except TypeError as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Password must be a string'
			return response, 401

		if not password_match:
			response['success'] = False
			if is_email(user_id):
				response['message'] = 'Email address or password incorrect.'
			elif is_mobile(user_id):
				response['message'] = 'Phone number or password incorrect.'
			else:
				response['message'] = 'Email or phone number required.'
			return response, 401
		if user.settings:
			if user.settings.use_mfa:
				# Force User to go to MFA Page.
				user.send_authy_request()
				response['success'] = True
				response['data'] = user.mfa_dict()
				return response, 200

		response['success'] = True
		response['data'] = user.to_dict()
		return response, 201

	def get_sudo(self):
		response = {}
		user = Users.get_user(email='idarlington@grit.systems')
		if user:
			response['success'] = True
			response['data'] = user.to_dict()
			return response, 201
		response['success'] = True
		response['message'] = 'Not found'
		return response, 404

	def logout(self, logout_payload):
		"""TODO"""
		pass

class SignUpManager(object):
	"""
		Class to manage user creation and retrieval
	"""

	def signup(self, payload):
		"""
			Method to create end users
			@args: data response
			@returns: a status message and status code
		"""
		response = {}
		user_email = payload['email']
		del payload['confirm_password']
		user = Users.get_user(email=user_email)
		if user is None:
			# User email has probably been tampered with
			response['success'] = False
			response['message'] = 'Invalid User'
			return response, 400
		# if not bool(user.invite_clicked):
		# 	response['success'] = False
		# 	response['message'] = 'Cannot Signup without clicking invite link in email.'
		# 	return response, 403
		try:
			user_created = user.create_user(**payload)
		except UUIDClashError as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'User Signup failed. Try again.'
			return response, 400

		user = Users.get_user(email=user_email)
		if user_created:
			if user is None:
				response['success'] = False
				response['message'] = 'User Signup failed. Try again.'
				return response, 400

			if user.settings.use_mfa:
				# Force User to go to MFA Page.
				user.send_authy_request()
				response['success'] = True
				response['data'] = user.mfa_dict()
				return response, 200

			response['success'] = True
			response['data'] = user.to_dict()
			return response, 201

		logger.warning('User {} Signup failed. Try again.'.format(user_email))
		response['success'] = False
		response['message'] = 'User Signup failed. Try again.'
		return response, 400
