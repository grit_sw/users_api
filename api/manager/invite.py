from api.models import Users


class InvitationManager:
    """
            Class to manage user creation and retrieval
    """

    @staticmethod
    def invite_params(user_id):
        response = {}
        user = Users.get_user(user_id=user_id)
        if user is None:
            response['success'] = False
            response['message'] = 'User {} Unauthourised'.format(user_id)
            return response, 401

        if not user.can_create_users():
            response['success'] = False
            response['message'] = 'User {} can\'t create users.'.format(
                user_id)
            return response, 403
        response['success'] = True
        response['data'] = user.invite_dict
        return response, 201

    @staticmethod
    def new_invite(payload):
        """
                Method to create end users
                @args: data response
                @returns: a status message and status code
        """
        response = {}
        email_count = len(payload['emails'])
        failed, success_count = Users.new_user(payload)
        failed_count = len(failed)
        if failed_count > 0:
            if failed_count < email_count:
                response['success'] = False
                response['status'] = '{} Emails out of {}were not created.'.format(
                    failed_count, email_count)
                response['failed_emails'] = failed
                return response, 400

        response['success'] = True
        response['message'] = '{} invites saved. Sending invite emails.'.format(
            success_count)
        return response, 200

    @staticmethod
    def check_status(user_id, email):
        """
                Method to create end users
                @args: data response
                @returns: a status message and status code
        """
        response = {}
        requester = Users.get_user(user_id=user_id)
        if requester is None:
            response['success'] = False
            response['message'] = 'Unauthourized'
            return response, 401

        user = Users.get_user(email=email)
        if user is None:
            response['success'] = False
            response['message'] = 'User not found'
            return response, 403

        status = user.invite_status()
        response['success'] = True
        response['data'] = status
        return response, 200

    @staticmethod
    def resend_invite(email):
        """
                Method to create end users
                @args: data response
                @returns: a status message and status code
        """
        response = {}
        user = Users.get_user(email=email)

        if user is None:
            response['success'] = False
            response['message'] = 'User not found'
            return response, 404

        if not user.invite_sent:
            response['success'] = False
            response['message'] = 'User not invited'
            return response, 403

        if user.signed_up:
            response['success'] = False
            response['message'] = 'User already signed up'
            return response, 409

        user.resend_invite()
        response['success'] = True
        response['message'] = 'Sending invite'
        return response, 200

    @staticmethod
    def set_invite_clicked(email):
        response = {}
        user = Users.get_user(email=email)
        if not user:
            response['success'] = False
            response['status'] = 'User not found'
            return response, 404

        Users.set_invite_clicked(user)
        # response = redirect(app.config['SIGNUP_PAGE'] + "?email={}".format(user.email))
        response['success'] = True
        response['email'] = user.email
        return response, 200
