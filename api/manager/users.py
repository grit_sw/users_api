from flask import request

from api.exceptions import UnableToDeleteError
from api.models import Users
from logger import logger


class UserManager:
    """
            Class to manage user creation and retrieval
    """

    @staticmethod
    def search(
            search_term,
            size,
            user_id,
            facility_id=None,
            search_role=None):
        """
                Method to get the details of an end user.
                A requester can only view the details of a user who is a direct or indirect child as well as his own details.
                @param: user_id: ID of the user whose details are to be viewed
                @param: viewer_id: ID of the user requesting the details
                @returns: a status message and status code
        """

        response = {}
        user = Users.get_user(user_id=user_id)
        if not user:
            response['success'] = False
            response['message'] = 'Not found'
            return response, 404

        if user.get_role_id < 3:
            response['success'] = False
            response['message'] = 'Unauthourised'
            return response, 403

        if search_role:
            try:
                search_role = int(search_role)
            except ValueError:
                response['success'] = False
                response['message'] = 'Bad search string'
                return response, 400

            if not facility_id:
                response['success'] = False
                response['message'] = 'Bad search string'
                return response, 400

        if facility_id:
            if not search_role:
                response['success'] = False
                response['message'] = 'Bad search string'
                return response, 400

        data, has_prev, has_next = Users.search_for(
            search_term, size, facility_id=facility_id, search_role=search_role)

        response['success'] = True
        response['data'] = data
        response['has_prev'] = has_prev
        response['has_next'] = has_next
        return response, 200

    @staticmethod
    def retrieve_user(user_id, viewer_id=None, role_id=None):
        """
                Method to get the details of an end user.
                A requester can only view the details of a user who is a direct or indirect child as well as his own details.
                @param: user_id: ID of the user whose details are to be viewed
                @param: viewer_id: ID of the user requesting the details
                @returns: a status message and status code
        """
        response = {}

        if not viewer_id and not role_id:
            response['success'] = True
            response['message'] = 'Invalid Request, role_id or viewer_id should be passed.'
            return response, 403

        if role_id:
            if int(role_id) == 6:
                user = Users.get_user(user_id=user_id)
                try:
                    user_details = user.to_dict()
                except AttributeError:
                    # user was not found
                    response['success'] = False
                    response['message'] = 'User could not be found'
                    return response, 403
                response['success'] = True
                response['data'] = user_details
                return response, 200

        requested = Users.get_user(user_id=user_id)
        requester = Users.get_user(user_id=viewer_id)

        if not requester:
            response['success'] = False
            response['message'] = 'You should not be here.'
            return response, 403

        if not requested:
            response['success'] = False
            response['message'] = 'User not found.'
            return response, 404

        if requester.is_a('grit_admin'):
            response['success'] = True
            response['data'] = requested.to_dict()
            return response, 200

        if requester == requested:
            response['success'] = True
            response['data'] = requester.to_dict()
            return response, 200

        if not requester.is_ancestor_of(requested):
            response['success'] = True
            response['message'] = 'Invalid Request.'
            return response, 403

        response['success'] = True
        response['data'] = requested.to_dict()
        return response, 200

    @staticmethod
    def delete_user(user_id, viewer_id):
        """
                Method to get the details of an end user.
                A requester can only view the details of a user who is a direct or indirect child as well as his own details.
                @param: user_id: ID of the user whose details are to be viewed
                @param: viewer_id: ID of the user requesting the details
                @returns: a status message and status code
        """

        response = {}
        to_delete = Users.get_user(user_id=user_id)

        if to_delete is None:
            response['success'] = False
            response['message'] = 'User Not Found'
            return response, 404

        deleter = Users.get_user(user_id=viewer_id)

        if deleter is None:
            response['success'] = False
            response['message'] = 'Invalid User'
            return response, 403

        logger.info('to_delete {}'.format(to_delete))
        logger.info('deleter {}'.format(deleter))
        logger.info('viewer_id {}'.format(viewer_id))

        if deleter.is_a('grit_admin'):
            try:
                deleted = Users.delete_user(to_delete.user_id)
                if deleted:
                    logger.info(
                        '{} deleted {}'.format(
                            deleter.user_id, user_id))
                    response['success'] = True
                    response['message'] = 'User Deleted'
                    return response, 200
            except UnableToDeleteError as e:
                logger.critical(e)
                response['success'] = False
                response['message'] = 'Unable to delete user {}'.format(
                    user_id)
                return response, 500

        if deleter.is_same(to_delete):
            if deleter.is_a('user'):
                try:
                    deleted = Users.delete_user(to_delete.user_id)
                    if deleted:
                        logger.info('{} deleted self'.format(user_id))
                        response['success'] = True
                        response['message'] = 'User Deleted'
                        return response, 200
                except UnableToDeleteError as e:
                    logger.critical(e)
                    response['success'] = False
                    response['message'] = 'Unable to delete your profile'
                    return response, 500
            else:
                response['success'] = False
                response['message'] = 'Forbidden'
                return response, 403

        if not deleter.in_same_facility(to_delete):
            response['success'] = False
            response['message'] = 'Forbidden'
            return response, 403

        if deleter.is_sibling_of(to_delete):
            response['success'] = False
            response['message'] = 'Forbidden'
            return response, 403

        if deleter.is_descendant_of(to_delete):
            response['success'] = False
            response['message'] = 'Forbidden'
            return response, 403

        if deleter.is_ancestor_of(to_delete):
            try:
                deleted = Users.delete_user(to_delete.user_id)
                if deleted:
                    logger.info(
                        '{} deleted {}'.format(
                            deleter.user_id, user_id))
                    response['success'] = True
                    response['message'] = 'User Deleted'
                    return response, 200
            except UnableToDeleteError as e:
                logger.critical(e)
                logger.info(
                    '{} was unable to delete user {}'.format(
                        deleter.email, user_id))
                response['success'] = False
                response['message'] = 'Deletion failed please try again later.'
                return response, 500

        response['success'] = False
        response['message'] = 'Forbidden'
        return response, 403

    @staticmethod
    def unmetered_users():
        # return shops that don't have a meter_id and customer_id
        # Note that a user can have more than one shop
        unmetered_users = Users.unmetered_users()
        return unmetered_users, 201

    @staticmethod
    def all_roles(user_id):
        response = {}
        user = Users.get_user(user_id=user_id)
        if user is None:
            response['success'] = False
            response['message'] = 'User Not Found'
            return response, 404

        if not user.is_a('grit_admin'):
            response['success'] = False
            response['message'] = 'Unauthourized'
            return response, 403

        resp = user.child_roles()
        response['success'] = True
        response['data'] = resp
        return response, 201

    @staticmethod
    def all_users_in_all_facilities_with_role(page, user_id, role_id):
        response = {}
        user = Users.get_user(user_id=user_id)
        if user is None:
            response['message'] = 'User Not Found'
            return response, 404

        if not user.is_a('grit_admin'):
            response['success'] = False
            response['message'] = 'Unauthourized'
            return response, 403

        try:
            users, has_prev, has_next = user.all_users_in_all_facilities_with_role(
                page, role_id)
            response['success'] = True
            response['data'] = users
            response['count'] = len(users)
            response['has_prev'] = has_prev
            response['has_next'] = has_next
            return response, 201

        except Exception as e:
            logger.exception(e)
            response['success'] = False
            response['message'] = 'Internal Server Error'
            return response, 500

    @staticmethod
    def all_users_in_one_facility_with_role(
            page, user_id, role_id, facility_id):
        response = {}
        user = Users.get_user(user_id=user_id)
        facility = Users.get_facility(facility_id)
        if user is None:
            response['message'] = 'User Not Found'
            return response, 404

        if facility is None:
            response['message'] = 'Facility Not Found'
            return response, 404

        user_facilities = user.facilities

        if int(role_id) < 5 and user.role_id != 5:
            facility_match = False
            if user_facilities:
                for facility in user_facilities:
                    if facility['facility_id'] == facility_id:
                        facility_match = True
                        break

            if not facility_match:
                response['success'] = False
                response['message'] = 'Forbidden'
                return response, 403

        if not user.is_child_role(role_id):
            response['success'] = False
            response['message'] = 'Role Not Found'
            return response, 403

        try:
            users, has_prev, has_next = user.all_users_in_one_facility_with_role(
                page, role_id, facility_id)
            response['success'] = True
            response['data'] = users
            response['count'] = len(users)
            response['has_prev'] = has_prev
            response['has_next'] = has_next
            return response, 200

        except Exception as e:
            logger.exception(e)
            response['success'] = False
            response['message'] = 'Internal Server Error'
            return response, 500

    @staticmethod
    def me(user_id):
        """
                Method to get a user's details
                @param: user_id: ID of the user
                @returns: response and status code
        """
        response = {}
        user = Users.get_user(user_id=user_id)
        if not user:
            response['success'] = False
            response['message'] = 'User not found'
            return response, 404
        response['success'] = True
        response['data'] = user.to_dict()
        return response, 200

    @staticmethod
    def edit_me(user_id, payload):
        """
                Method to get a user's details
                @param: user_id: ID of the user
                @returns: response and status code
        """
        response = {}
        user = Users.get_user(user_id=user_id)
        if not user:
            response['success'] = False
            response['message'] = 'User not found'
            return response, 404
        del payload['user_id']
        user_dict = user.edit_me(**payload)
        response['success'] = True
        response['data'] = user_dict
        return response, 200

    @staticmethod
    def edit_user(payload):
        """
                Method to get a user's details
                @param: user_id: ID of the user
                @returns: response and status code
        """
        response = {}

        requester_id = request.cookies.get("user_id")
        requester = Users.get_user(user_id=requester_id)
        role_id = request.cookies.get('role_id')

        if not requester and not role_id:
            logger.info("Invalid edit user action attempted by anonymous user")
            response['success'] = False
            response['message'] = 'Unauthourised'
            return response, 403

        if requester and requester.get_role_id not in {3, 4, 5}:
            logger.info(
                "Invalid edit user action attempted by user {}".format(
                    requester.to_dict()))
            response['success'] = False
            response['message'] = 'Unauthourised'
            return response, 403

        if int(role_id) != 6:
            logger.info("Invalid edit user action attempted by anonymous user")
            response['success'] = False
            response['message'] = 'Unauthourised'
            return response, 403

        user_to_edit = payload.get('user_id')
        user = Users.get_user(user_id=user_to_edit)
        if not user:
            response['success'] = False
            response['message'] = 'User not found'
            return response, 404
        del payload['user_id']
        user_dict = user.edit_user(**payload)
        response['success'] = True
        response['data'] = user_dict
        return response, 200

    @staticmethod
    def all_users(page, user_id):
        """
                Method to get end users associated with a client admin
                @param: client_admin_id: ID of the customer's client admin
                @param: customer_id: ID of the customer
                @returns: a status message and status code
        """
        response = {}
        user = Users.get_user(user_id=user_id)
        if not user:
            response['success'] = False
            response['message'] = 'User not found'
            return response, 404

        if not user.is_a('grit_admin'):
            # todo: Manage roles the way they are meant to be managed in microservices.
            # IRDK how to go about that.
            response['success'] = False
            response['message'] = 'Forbidden'
            return response, 403

        users, has_prev, has_next = Users.all_users(page, user_id)
        try:
            response['success'] = True
            response['data'] = users
            response['count'] = len(users)
            response['has_prev'] = has_prev
            response['has_next'] = has_next
        except Exception as e:
            logger.exception(e)
            response['success'] = False
            response['message'] = 'Internal Server Error'
            return response, 500

        return response, 200

    @staticmethod
    def all_config_users(page, user_id):
        """
                Method to get end users associated with a client admin
                @param: client_admin_id: ID of the customer's client admin
                @param: customer_id: ID of the customer
                @returns: a status message and status code
        """
        response = {}
        user = Users.get_user(user_id=user_id)
        if not user:
            response['success'] = False
            response['message'] = 'User not found'
            return response, 404

        if not user.is_a('grit_admin'):
            # todo: Manage roles the way they are meant to be managed in microservices.
            # IRDK how to go about that.
            response['success'] = False
            response['message'] = 'Forbidden'
            return response, 403

        users, has_prev, has_next = Users.all_config_users(page)
        try:
            response['success'] = True
            response['data'] = users
            response['count'] = len(users)
            response['has_prev'] = has_prev
            response['has_next'] = has_next
        except Exception as e:
            logger.exception(e)
            response['success'] = False
            response['message'] = 'Internal Server Error'
            return response, 500

        return response, 200

    @staticmethod
    def all_users_in_a_facility(page, user_id, facility_id):
        """
                Method to get all the users in a facility.
                @param: user_id: ID of the user making the request.
                @param: facility_id: ID of the facility
                @param: page: page number of the request
                @returns: a response payload and status code
        """
        response = {}
        user = Users.get_user(user_id=user_id)
        facility = Users.get_facility(facility_id)

        if not facility:
            response['success'] = False
            response['message'] = 'Not found.'
            return response, 404

        if not user:
            response['success'] = False
            response['message'] = 'Unauthourised'
            return response, 403

        if not user.can_create_users():
            response['success'] = False
            response['message'] = 'Unauthourized'
            return response, 403

        users, has_prev, has_next = Users.all_users_in_a_facility(
            page, user_id, facility_id)
        try:
            response['success'] = True
            response['data'] = users
            response['count'] = len(users)
            response['has_prev'] = has_prev
            response['has_next'] = has_next
            return response, 200

        except Exception as e:
            logger.critical(e)
            response['success'] = False
            response['message'] = 'Internal Server Error'
            return response, 500

    @staticmethod
    def all_users_in_a_group(page, user_id, group_id):
        response = {}
        user = Users.get_user(user_id=user_id)
        if user is None:
            response['success'] = False
            response['message'] = 'User Not Found'
            return response, 403

        if (user.get_group_id != group_id) and not user.is_a('grit_admin'):
            response['success'] = False
            response['message'] = 'User Not a member of the group'
            return response, 403

        users, has_prev, has_next = Users.all_users_in_a_group(page, group_id)
        try:
            response['success'] = True
            response['data'] = users
            response['count'] = len(users)
            response['has_prev'] = has_prev
            response['has_next'] = has_next
            return response, 200
        except Exception as e:
            logger.critical(e)
            response['success'] = False
            response['message'] = 'Internal Server Error'
            return response, 500

    @staticmethod
    def max_admin_in_group(group_id):
        response = {}
        group = Users.get_user_group(group_id)
        if group is None:
            response['success'] = False
            response['message'] = 'User Group Not Found'
            return response, 404

        try:
            admin = Users.max_admin_in_group(group_id)
            response['success'] = True
            response['data'] = admin
            return response, 200
        except Exception as e:
            logger.exception(e)
            response['success'] = False
            response['message'] = 'Internal Server Error'
            return response, 500

    @staticmethod
    def change_user_role(performer_id, user_id, role_id):
        response = {}
        requester = Users.get_user(user_id=performer_id)
        requested = Users.get_user(user_id=user_id)

        if not requester:
            response['success'] = False
            response['message'] = 'You should not be here.'
            return response, 403

        if not requested:
            response['success'] = False
            response['message'] = 'User not found.'
            return response, 404

        if int(role_id) > requester.get_role_id:
            response['success'] = False
            response['message'] = 'Forbidden.'
            return response, 403

        if requester.get_role_id < requested.get_role_id:
            response['success'] = False
            response['message'] = 'Forbidden.'
            return response, 403

        if not requester.in_same_facility(requested):
            response['success'] = False
            response['message'] = 'Forbidden.'
            return response, 403

        if not requester.is_a('grit_admin'):
            if not requested.is_descendant_of(requester):
                response['success'] = False
                response['message'] = 'Forbidden.'
                return response, 403

        user_dict = requested.change_user_role(role_id)
        response['success'] = True
        response['data'] = user_dict
        return response, 200

    @staticmethod
    def change_password(user_id, payload):
        response = {}
        user = Users.get_user(user_id=user_id)

        if not user:
            response['success'] = False
            response['message'] = 'User not found.'
            return response, 404

        del payload['confirm_password']
        user_dict = user.change_password(**payload)
        response['success'] = True
        response['data'] = user_dict
        return response, 200

    @staticmethod
    def create_fake_user(data):
        response = {}
        users = data['users']
        print(users)
        new_users = []
        for user in users:
            email = user['invite']['email']
            user['signup']['email'] = email
            user_exists = Users.get_user(email=email)
            if user_exists:
                new_users.append(user_exists.to_dict())
                continue
            invited_user = Users(**user['invite'])
            invited_user.create_user(**user['signup'])
            new_users.append(invited_user.to_dict())

        print(new_users)
        response['success'] = True
        response['data'] = new_users
        return response, 201
