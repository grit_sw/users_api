from api.models import Users


class MFAManager:
    """docstring for MFAManager"""

    @staticmethod
    def enable_mfa(user_id, admin_id=None):
        response = {}
        if admin_id is not None:
            admin_user = Users.get_user(user_id=admin_id)
            if not admin_user.can_create_users():
                response['success'] = False
                response['message'] = 'Unauthourised.'
                return response, 403

        user = Users.get_user(user_id=user_id)
        if user is None:
            response['success'] = False
            response['message'] = 'User not found.'
            return response, 404

        if user.settings.use_mfa:
            response['success'] = False
            response['message'] = 'MFA Already Enabled.'
            return response, 409
        mfa_enabled = user.enable_mfa()

        if mfa_enabled:
            response['success'] = True
            response['message'] = 'Multi-factor authentication enabled.'
            return response, 200
        response['success'] = False
        response['message'] = 'Error during registration. Please try again later.'
        return response, 400

    @staticmethod
    def disable_mfa(user_id, admin_id=None):
        response = {}
        if admin_id is not None:
            admin_user = Users.get_user(user_id=admin_id)
            if not admin_user.can_create_users():
                response['success'] = False
                response['message'] = 'Unauthourised.'
                return response, 403

        user = Users.get_user(user_id=user_id)
        if user is None:
            response['success'] = False
            response['message'] = 'User not found.'
            return response, 404

        if not user.settings.use_mfa:
            response['success'] = False
            response['message'] = 'MFA not Enabled.'
            return response, 409

        mfa_disabled = user.disable_mfa()

        if mfa_disabled:
            response['success'] = True
            response['message'] = 'Multi-factor authentication disabled.'
            return response, 200

        response['success'] = False
        response['message'] = 'Error during registration. Please try again later.'
        return response, 400


class KeypadManager:
    """docstring for KeypadManager"""

    @staticmethod
    def enable_keypad(user_id, admin_id=None):
        response = {}
        if admin_id is not None:
            admin_user = Users.get_user(user_id=admin_id)
            if not admin_user.can_create_users():
                response['success'] = False
                response['message'] = 'Unauthourised.'
                return response, 403

        user = Users.get_user(user_id=user_id)
        if user is None:
            response['success'] = False
            response['message'] = 'User not found.'
            return response, 404

        if user.settings.use_keypad:
            response['success'] = False
            response['message'] = 'Keypad is already being used.'
            return response, 409
        keypad_enabled = user.enable_keypad()

        if keypad_enabled:
            response['success'] = True
            response['message'] = 'Keypad usage enabled.'
            return response, 200

        response['success'] = False
        response['message'] = 'Error during registration. Please try again later.'
        return response, 400

    @staticmethod
    def disable_keypad(user_id, admin_id=None):
        response = {}
        if admin_id is not None:
            admin_user = Users.get_user(user_id=admin_id)
            if not admin_user.can_create_users():
                response['success'] = False
                response['message'] = 'Unauthourised.'
                return response, 403

        user = Users.get_user(user_id=user_id)
        if user is None:
            response['success'] = False
            response['message'] = 'User not found.'
            return response, 404

        if not user.settings.use_keypad:
            response['success'] = False
            response['message'] = 'Keypad not being used.'
            return response, 409

        keypad_disabled = user.disable_keypad()

        if keypad_disabled:
            response['success'] = True
            response['message'] = 'Keypad usage disabled.'
            return response, 200

        response['success'] = False
        response['message'] = 'Error during registration. Please try again later.'
        return response, 400
