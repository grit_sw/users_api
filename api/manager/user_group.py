from api.models import UserGroup


class UserGroupManager:
    """
            Class to manage user creation and retrieval
    """
    @staticmethod
    def retrieve_user_group(
            role_id,
            group_id=None,
            blockchain_address=None):
        """
                Method to get the details of a user group
        """
        allowed_roles = (5, 6)
        response = {}
        try:
            role_id_int = int(role_id)
        except ValueError:
            response['success'] = False
            response['message'] = f"role_id:{role_id} could not converted to a number"
            return response, 403

        if role_id_int not in allowed_roles:
            response['success'] = False
            response['message'] = 'You should not be here.'
            return response, 403

        if group_id:
            user_group = UserGroup.get_group(group_id=group_id)
        elif blockchain_address:
            user_group = UserGroup.get_group(
                blockchain_address=blockchain_address)
        else:
            response['success'] = False
            response['message'] = "blockchain_address or group_id should passed in as None"
            return response, 403

        try:
            user_group_details = user_group.to_dict()
        except AttributeError:
            response['success'] = False
            response['message'] = 'User Group could not be found'
            return response, 404
        response['success'] = True
        response['data'] = user_group_details
        return response, 200

    @staticmethod
    def edit_group(payload, role_id):
        """
                Method to get a user's details
                @param: user_id: ID of the user
                @returns: response and status code
        """
        allowed_roles = (5, 6)
        response = {}
        try:
            role_id_int = int(role_id)
        except ValueError:
            response['success'] = False
            response['message'] = f"role_id:{role_id} could not converted to a number"
            return response, 403

        if role_id_int not in allowed_roles:
            response['success'] = False
            response['message'] = 'Unauthourised'
            return response, 403

        user_group = UserGroup.get_group(group_id=payload.get('group_id'))
        if not user_group:
            response['success'] = False
            response['message'] = 'UserGroup not found'
            return response, 404

        del payload['group_id']
        user_group_dict = user_group.edit_group(**payload)
        response['success'] = True
        response['data'] = user_group_dict
        return response, 200

    @staticmethod
    def all_groups(role_id):
        allowed_roles = (5, 6)
        response = {}
        try:
            role_id_int = int(role_id)
        except ValueError:
            response['success'] = False
            response['message'] = f"role_id:{role_id} could not converted to a number"
            return response, 403

        if role_id_int not in allowed_roles:
            response['success'] = False
            response['message'] = 'Unauthourised'
            return response, 403

        user_groups = UserGroup.get_all()
        response['success'] = True
        response['data'] = user_groups
        return response, 200
