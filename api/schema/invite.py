from collections import namedtuple

from marshmallow import (Schema, ValidationError, fields, post_load, validates,
                         validates_schema)

from api import mongo_users
from api.exceptions import NotFoundError
from api.models import Users


NewInvite = namedtuple('NewInvite', [
    'role_id',
    'mfa_required',
    'gtg_user',
    'disco_user',
    'configuration_user',
    'keypad_user',
    'user_id',
    'emails',
    'facility_id',
])

Email = namedtuple('Emails', [
    'email',
])


class EmailSchema(Schema):
    """docstring for EmailSchema"""
    email = fields.Email(required=True)

    @staticmethod
    @post_load
    def create_email(data):
        return Email(**data)

    @staticmethod
    @validates('email')
    def validate_email(value):
        user = Users.get_user(email=value)
        if user:
            if bool(user.signed_up):
                raise ValidationError(
                    'User {} already signed up'.format(value))

    @staticmethod
    @validates_schema(pass_original=True)
    def validate_match(data, original_data):
        print(data)
        print(original_data)


class InvitationSchema(Schema):
    role_id = fields.String(required=True)
    mfa_required = fields.Boolean(required=True)
    gtg_user = fields.Boolean(required=True)
    disco_user = fields.Boolean(required=True)
    configuration_user = fields.Boolean(required=True)
    keypad_user = fields.Boolean(required=True)
    facility_id = fields.String(required=True)
    user_id = fields.String(required=True)
    emails = fields.List(fields.Email(), required=True)

    @staticmethod
    @post_load
    def create_invite(data):
        return NewInvite(**data)

    @staticmethod
    @validates_schema(pass_original=True)
    def validate_match(data, original_data):
        print(data)
        print(original_data)
        role_id = original_data[0].get('role_id')
        user_id = original_data[0].get('user_id')
        # mfa_required = original_data[0].get('mfa_required')
        # gtg_user = original_data[0].get('gtg_user')
        # configuration_user = original_data[0].get('configuration_user')
        # keypad_user = original_data[0].get('keypad_user')
        emails = original_data[0].get('emails')
        facility_id = original_data[0].get('facility_id')
        # if not role_id:
        # 	raise ValidationError('User Role Required')
        # if mfa_required is None:
        # 	raise ValidationError('MFA info required')
        # if gtg_user is None:
        # 	raise ValidationError('GTG user info required')
        # if keypad_user is None:
        # 	raise ValidationError('Keypad user info required')
        # if configuration_user is None:
        # 	raise ValidationError('Configuration user info required')
        user = Users.get_user(user_id=user_id)
        if not user:
            raise NotFoundError('User not Logged In')
        child_ids = {int(role['id']) for role in user.child_roles()}
        facilities = {facility['facility_id'] for facility in user.facilities}
        try:
            role_id = int(role_id)
        except ValueError:
            raise ValidationError('Invalid Input')
        if role_id not in child_ids:
            raise ValidationError('Invalid Role')
        if facility_id not in facilities:
            raise ValidationError('Invalid Facility')
        if role_id == 4:
            for invite in emails:
                print(invite)
                mongo_user = mongo_users.find_one({'email': invite['email']})
                if not mongo_user:
                    raise ValidationError(
                        'Dashboard Profile Not Found for {}'.format(
                            invite['email']))
