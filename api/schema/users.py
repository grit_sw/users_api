from collections import namedtuple

from marshmallow import Schema, ValidationError, fields, post_load, validates, validates_schema

from api.common import is_email, is_mobile
from api.models import Users


ConfirmPassword = namedtuple('ConfirmPassword', [
    'new_password',
    'confirm_password',
])

EditMe = namedtuple('EditMe', [
    'user_id',
    'first_name',
    'last_name',
    'email',
    'phone_number',
    'use_mfa',
])

EditUser = namedtuple('EditUser', [
    'user_id',
    'first_name',
    'last_name',
    'email',
    'phone_number',
    'use_mfa',
    'use_keypad',
])


class EditMeSchema(Schema):
    user_id = fields.String(required=True)
    first_name = fields.String(required=True)
    last_name = fields.String(required=True)
    email = fields.String(required=True)
    phone_number = fields.String(required=True)
    use_mfa = fields.Boolean(required=True)

    @staticmethod
    @post_load
    def new_view(data):
        return EditMe(**data)

    @staticmethod
    @validates('user_id')
    def validate_user_id(value):
        user = Users.get_user(user_id=value)
        if not user:
            raise ValidationError('User not found')

    @staticmethod
    @validates('first_name')
    def validate_first_name(value):
        if not value:
            raise ValidationError('First name required.')

    @staticmethod
    @validates('last_name')
    def validate_last_name(value):
        if not value:
            raise ValidationError('Last name required.')

    @staticmethod
    @validates('email')
    def validate_email(value):
        if not value:
            raise ValidationError('Email address required.')
        if not is_email(value):
            raise ValidationError('Invalid email address')

    @staticmethod
    @validates('phone_number')
    def validate_phone_number(value):
        if not value:
            raise ValidationError('Phone number required.')
        if not is_mobile(value):
            raise ValidationError('Invalid phone number')

    @staticmethod
    @validates('use_mfa')
    def validate_use_mfa(value):
        if not isinstance(value, bool):
            raise ValidationError('Invalid response')


class EditUserSchema(Schema):
    user_id = fields.String(required=True)
    first_name = fields.String(required=True)
    last_name = fields.String(required=True)
    email = fields.String(required=True)
    phone_number = fields.String(required=True)
    use_mfa = fields.Boolean(required=True)
    use_keypad = fields.Boolean(required=True)

    @staticmethod
    @post_load
    def edit(data):
        return EditUser(**data)

    @staticmethod
    @validates('user_id')
    def validate_user_id(value):
        user = Users.get_user(user_id=value)
        if not user:
            raise ValidationError('User not found')

    @staticmethod
    @validates('first_name')
    def validate_first_name(value):
        if not value:
            raise ValidationError('First name required.')

    @staticmethod
    @validates('last_name')
    def validate_last_name(value):
        if not value:
            raise ValidationError('Last name required.')

    @staticmethod
    @validates('email')
    def validate_email(value):
        if not value:
            raise ValidationError('Email address required.')
        if not is_email(value):
            raise ValidationError('Invalid email address')

    @staticmethod
    @validates('phone_number')
    def validate_phone_number(value):
        if not value:
            raise ValidationError('Phone number required.')
        if not is_mobile(value):
            raise ValidationError('Invalid phone number')

    @staticmethod
    @validates('use_mfa')
    def validate_use_mfa(value):
        if not isinstance(value, bool):
            raise ValidationError('Invalid response')

    @staticmethod
    @validates('use_keypad')
    def validate_use_keypad(value):
        if not isinstance(value, bool):
            raise ValidationError('Invalid response')


class ConfirmPasswordSchema(Schema):
    new_password = fields.String(required=True)
    confirm_password = fields.String(required=True)

    @staticmethod
    @post_load
    def set_password(data):
        return ConfirmPassword(**data)

    @staticmethod
    @validates('new_password')
    def validate_new_password(value):
        if not value:
            raise ValidationError("Missing data for required field.")

    # thoughts: I have a feeling that methods like this one as the one in
    # manager and controllers would fail
    @staticmethod
    @validates('confirm_password')
    def validate_confirm_password(value):
        if not value:
            raise ValidationError("Missing data for required field.")

    @staticmethod
    @validates_schema(pass_original=True)
    def validate_match(_, original_data):
        if original_data['new_password'] != original_data['confirm_password']:
            raise ValidationError('Passwords don\'t match')
