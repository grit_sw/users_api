from collections import namedtuple

from marshmallow import Schema, fields, post_load, validates, ValidationError

from api.common import is_email
from api.models import Facility

from logger import logger

NewFacility = namedtuple('NewFacility', [
    'name',
    "country",
    "state",
    "city",
    "street",
    "house_no",
    "facility_type",
    "billing_method",
    "billing_unit",
    "invite_clients",
])


class NewFacilitySchema(Schema):
    name = fields.String(required=True)
    country = fields.String(required=True)
    state = fields.String(required=True)
    city = fields.String(required=True)
    street = fields.String(required=True)
    house_no = fields.String(required=True)
    facility_type = fields.String(required=True)
    billing_method = fields.String(required=True)
    billing_unit = fields.String(required=True)
    invite_clients = fields.List(
        fields.Dict(
            keys=fields.String(),
            values=fields.String()),
        missing=[])

    @staticmethod
    @post_load
    def new_facility(data):
        return NewFacility(**data)

    @staticmethod
    @validates('name')
    def validate_name(value):
        if not value:
            raise ValidationError('Facility Name Required')
        if Facility.exists(value):
            raise ValidationError('Facility {} Exists'.format(value))

    @staticmethod
    @validates('facility_type')
    def validate_type_of(value):
        if not value:
            raise ValidationError('Facility Type Required')

    @staticmethod
    @validates('invite_clients')
    def validate_invite_clients(value):
        logger.info(f"Invite clients is {value}")
        for client in value:
            try:
                user_role = client['role']
                email = client['email']
            except KeyError:
                raise ValidationError(
                    "role and email should be keys in every json passed")
            try:
                int(user_role)
            except (TypeError, ValueError):
                raise ValidationError(
                    "The first item (user_role) could not be converted to a number")

            if not is_email(email):
                raise ValidationError(f"Email {email} is not a valid email")
