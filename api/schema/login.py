from collections import namedtuple

from marshmallow import Schema, fields, post_load, validates, ValidationError

from api.common import is_email, is_mobile
from api.schema.common import validate_password

UserLogin = namedtuple('UserLogin', ['user_id', 'password'])


class LoginSchema(Schema):
    user_id = fields.String(required=True)
    password = fields.String(required=True, validate=validate_password)

    @staticmethod
    @post_load
    def login_user(data):
        return UserLogin(**data)

    @staticmethod
    @validates('user_id')
    def validate_user_id(value):
        if not (is_email(value) or is_mobile(value)):
            raise ValidationError('Email or Phone Number Required')

    @staticmethod
    @validates('password')
    def validate_password(value):
        if not value:
            raise ValidationError('Password Required')

    def __repr__(self):
        return "LoginSchema <{}>".format(self.user_id)
