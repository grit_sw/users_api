from collections import namedtuple

from marshmallow import Schema, fields, post_load, validates, ValidationError

from api.models import Users
from logger import logger


AuthyCode = namedtuple('AuthyCode', ['user_id', 'authy_code'])


class AuthySchema(Schema):
    user_id = fields.String(required=True)
    authy_code = fields.String(required=True)

    @staticmethod
    @post_load
    def load_authy_code(data):
        return AuthyCode(**data)

    @staticmethod
    @validates('user_id')
    def validate_user_id(value):
        if not value:
            raise ValidationError('UserID Missing.')
        user = Users.get_user(user_id=str(value))
        if user is None:
            raise ValidationError('User Not Found.')

    @staticmethod
    @validates('authy_code')
    def validate_authy_code(value):
        try:
            int(value)
        except ValueError as e:
            logger.exception(e)
            raise ValidationError('Only digits accepted.')

        if not value:
            raise ValidationError('Authy Code Required.')

    def __repr__(self):
        return "AuthySchema <{}>".format(self.user_id)
