from collections import namedtuple
import re

from marshmallow import Schema, fields, post_load, validates, ValidationError, validates_schema

from api.models import Users
from api.schema.common import validate_password


UserLogin = namedtuple('UserLogin', ['user_id', 'password'])
NewUser = namedtuple('NewUser', [
    'first_name',
    'last_name',
    'email',
    'password',
    'confirm_password',
    'phone_number',
])


def is_email(email):
    if re.match(r"[^@]+@[^@]+\.[^@]+", email):
        return True
    return False


def is_grit_mail(email):
    if re.match(r"[^@]+@grit.systems", email):
        return True
    return False


def is_mobile(phone_number):
    # from: https://stackoverflow.com/a/12784477/4909255
    reg_string = r"^(?=(?:.{11}|.{14})$)[+]?[0-9]*$"
    if re.match(reg_string, phone_number):
        return True
    return False


class SignUpSchema(Schema):
    first_name = fields.String(required=True)
    last_name = fields.String(required=True)
    email = fields.Email(required=True)
    password = fields.String(required=True, validate=validate_password)
    confirm_password = fields.String(required=True)
    phone_number = fields.String(required=True)

    @staticmethod
    @post_load
    def create_user(data):
        return NewUser(**data)

    @staticmethod
    @validates('first_name')
    def validate_first_name(value):
        if not value:
            raise ValidationError('First Name Required')
        if len(value) >= 120:
            raise ValidationError('Name too long')

    @staticmethod
    @validates('last_name')
    def validate_last_name(value):
        if not value:
            raise ValidationError('Last Name Required')
        if len(value) >= 120:
            raise ValidationError('Name too long')

    @staticmethod
    @validates('email')
    def validate_email(value):
        if not value:
            raise ValidationError('Email required')
        if len(value) >= 70:
            raise ValidationError('Email too long')
        if not is_email(value):
            raise ValidationError('Invalid Email')
        user = Users.get_user(email=value)
        if user:
            if user.email != value:
                raise ValidationError('User email invalid.')
            if user.signed_up:
                raise ValidationError('User Already Signed Up.')
        if not user:
            raise ValidationError('User not found.')

    @staticmethod
    @validates('phone_number')
    def validate_phone_number(value):
        if not value:
            raise ValidationError('Phone Number Required')
        if not is_mobile(value):
            raise ValidationError('Invalid Phone Number')
        mobile = Users.get_user(phone_number=value)
        if mobile:
            raise ValidationError('User with phone number exists.')
        if mobile:
            if mobile.signed_up:
                raise ValidationError('User Already Signed Up.')

    @staticmethod
    @validates_schema(pass_original=True)
    def validate_match(data, original_data):
        print("\n\n\nOriginal Data = ", original_data)
        print(data)
        if original_data['password'] != original_data['confirm_password']:
            raise ValidationError('Passwords don\'t match')
