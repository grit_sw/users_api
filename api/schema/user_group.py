from collections import namedtuple

from marshmallow import Schema, fields, post_load


EditUserGroup = namedtuple('EditUserGroup', [
    'group_id',
    'blockchain_address',
    'blockchain_private_key'
])


class EditUserGroupSchema(Schema):
    group_id = fields.String(required=True)
    blockchain_address = fields.String(required=True)
    blockchain_private_key = fields.String(required=True)

    @staticmethod
    @post_load
    def new_view(data):
        return EditUserGroup(**data)
