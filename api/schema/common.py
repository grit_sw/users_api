from marshmallow import ValidationError


def validate_password(value):
    if not value:
        raise ValidationError('Password Required')
