from threading import Thread

import arrow

from flask import render_template
from flask_mail import Message

from api import mail, app, db
from api.models import users
from logger import logger


def send_async_invite(current_app, msg):
    with current_app.app_context():
        try:
            mail.send(msg)
            logger.info('Sent e-mail to {}'.format(msg.recipients))
            for recipient in msg.recipients:
                receiver = users.Users.get_user(email=recipient)
                receiver.invite_sent = True
                receiver.invite_sent_date = arrow.now().datetime
                receiver.invite_clicked = None
                db.session.add(receiver)
            db.session.commit()
        except Exception as e:
            logger.exception(e)


def send_email(subject, sender, recipients, text_body, html_body):
    msg = Message(subject, sender=sender, recipients=recipients)
    msg.body = text_body
    msg.html = html_body

    Thread(target=send_async_invite, args=(app, msg)).start()


def send_invite_email(receiver, sender=None, expires_in=None):
    if expires_in is None:
        try:
            expires_in = app.config['INVITE_EXPIRES']
        except Exception as e:
            logger.exception(e)
            logger.info('Invite Expiry time not set as INVITE_EXPIRES')

    token = receiver.create_invite_token(int(expires_in))
    try:
        host_url = app.config['HOST_URL']
        invite_accept_path = '/invite/accept/{}/'.format(token)
        signup_link = host_url + invite_accept_path
    except KeyError as e:
        logger.exception(e)
        logger.info('HOST_URL not found')
    # signup_link = url_for(
    #     'invite_accept',
    #     token=token,
    #     _external=True
    # )
    if sender is None:
        sender = app.config['ADMINS'][0]
    else:
        sender = sender.email
    send_email(subject='[GTG] Monitor Your Electricity Usage Today',
                       sender=app.config['ADMINS'][0],
                       recipients=[receiver.email],
                       text_body=render_template(
                           'invite_email.txt',
                           sender=sender,
                           signup_link=signup_link,
                       ),
               html_body=render_template(
                           'invite_email.html',
                           sender=sender,
                           signup_link=signup_link,
                       ),
               )
