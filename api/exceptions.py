class NotFoundError(AttributeError):
    """Inheriting from AttributError since sqlalchemy returns None if a user is not found"""


class FacilityError(AttributeError):
    """Inheriting from AttributError since sqlalchemy returns None if a user is not found"""


class UUIDClashError(ValueError):
    """Inheriting from AttributError since sqlalchemy returns None if a user is not found"""


class AuthourisationError(ValueError):
    """docstring for AuthourisationError"""


class RoleNotFoundError(NotFoundError):
    """Occurs when role is not found"""


class KafkaConnectionError(Exception):
    """Occurs when prosumer cannot connect to kafka broker."""


class TopicMissingError(Exception):
    """Error when topic is missing"""


class ValueSchemaNotFound(Exception):
    """Error when valueschema is not found"""


class UnableToDeleteError(Exception):
    """Occurs when a delete operation in the database was not successful"""
