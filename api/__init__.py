from werkzeug.contrib.fixers import ProxyFix
from pymongo import MongoClient

from flask import Flask
from flask_restplus import Api
from flask_sqlalchemy import SQLAlchemy
from flask_mail import Mail

from config import config


authorizations = {
    'apikey': {
        'type': 'apiKey',
        'in': 'header',
                'name': 'X-API-KEY'
    }
}

db = SQLAlchemy(session_options={"autoflush": False})
api = Api(doc='/doc/', authorizations=authorizations)
mail = Mail()
# client = MongoClient('mongodb://dashboard.grit.systems:27017/')
client = MongoClient('localhost', 27017)
mongodb = client['lift-mongo-app']
# probes = mongodb['probes']
mongo_users = mongodb['user.users']
# configurations = mongodb['configurations']
# print(users.find({}).count())
# print(users.find_one({'email': 'femi@sunhive.com'}))
# for u in users.find({}):
#     print(u['email'])
# break


app = Flask(__name__)


def create_api(config_name):
    app.wsgi_app = ProxyFix(app.wsgi_app)

    try:
        init_config = config[config_name]()
    except KeyError:
        raise Exception()
    except Exception:
        # For unforseen exceptions
        raise Exception()
        # exit()

    print('Running in {} Mode'.format(init_config))
    config_object = config.get(config_name)

    app.config.from_object(config_object)
    config_object.init_app(app)

    db.init_app(app)

    from api.controllers import auth_api as ns1
    from api.controllers import authy_api as ns2
    from api.controllers import invite_api as ns3
    from api.controllers import users_api as ns4
    from api.controllers import facility_api as ns5
    from api.controllers import user_settings_api as ns6
    from api.controllers import users_group_api as ns7
    api.add_namespace(ns1, path='/auth')
    api.add_namespace(ns2, path='/authy')
    api.add_namespace(ns3, path='/invite')
    api.add_namespace(ns4, path='/users')
    api.add_namespace(ns5, path='/facility')
    api.add_namespace(ns6, path='/user-settings')
    api.add_namespace(ns7, path='/user-groups')
    mail.init_app(app)
    api.init_app(app)

    with app.app_context():
        # db.reflect()
        # db.drop_all()
        db.create_all()

    return app
